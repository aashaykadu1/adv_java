# Advanced Java

## Module
* Servlets
* JSP
* Spring
* Hibernate
* Spring Boot

## Schedules
* 9.00 am to 1.30 pm -- Lecture
* 2.30 pm to 3.30 pm -- Q & A (Optional)
* 3.30 pm to 4.00 pm -- Lab Briefing
* 4.00 pm to 7.xx pm -- Lab

## Agenda
* Maven - Java Build Tool
* Maven - Web Project
* JSP Revision (Java beans + JSTL)
* Spring Concepts

## Maven

### Java Build Tool
* Ant -- Script to compile and deploy applications
* Maven -- Project build configuration -- pom.xml
* Gradle -- Project build configuration -- build.gradle
    * Android Studio - Android Project -- Gradle tool

## JSP Revision

## Classwork -- tutorialjsp
* step 0.1. Create Maven project for Online Tutorials project using Maven. Follow steps in screenshots. 
(Select WAR application). Create Deployment descriptor (web.xml).
* step 0.2. In pom.xml change Java version to 1.8 or 11 (as appropriate). Update the Maven project.
* step 0.3. In pom.xml add dependencies mysql, Java EE and JSTL.
* step 0.4. Copy POJO, DAO and DbUtils class from the uploaded project.
* step 1. Create index.jsp with link to login page.
* step 2. Create login.jsp and accept email and password.
* step 3. Create UserBean class and validate user there.
* step 4. Create validate.jsp, use UserBean in it and authenticate user. Redirect user to appropriate pages (as per diagram).
* step 5. Create TopicsBean to fetch list of topics.
* step 6. Create topics.jsp, use TopicsBean in it and display topics radio buttons.
* step 7. Create TopicTutorialsBean to fetch list of tutorials of the topic selected in previous page.
* step 8. Create tutorials.jsp, use TopicTutorialsBean in it and display tutorial links.

## Assignment -- tutorialjsp
* step 9. Create DetailsBean to fetch details of given tutorial.
* step 10. Create details.jsp, use DetailsBean in it and display details of selected tutorial. In this page display "Topics" page link for customer login and "Manage" page link for admin login (using JSTL tags). You can access user role if user is in session scope. (Hint: Add UserBean in session scope in validate.jsp, if not done already).
* step 11. Create logout.jsp to invalidate user session and display link back to login page.
    `<% session.invalidate(); %>`
* step 12. Create TutorialBean to fetch all tutorials list.
* step 13. Create manage.jsp, use TutorialBean in it and display all tutorial list in tabular format. Upon "name" click user should see details (on details.jsp). Add "Delete" action link to delete the tutorial. Also provide link to Sign Out.
* step 14. Create TutorialDelBean to delete given tutorial (by id).
* step 15. Create del.jsp, use TutorialDelBean in it to delete the bean and forward back to manage.jsp.

## Optional Assignment -- tutorialjsp
* step 16 (Optional). In login.jsp provide link for Sign Up. Implement registration functionality with appropriate beans and JSP pages.
* step 17 (Optional). In manage.jsp add link for Edit. Implement tutorial edit functionality with appropriate beans and JSP pages.