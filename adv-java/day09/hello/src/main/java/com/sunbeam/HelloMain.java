package src.main.java.com.sunbeam;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class HelloMain {
	private static final String DB_URL = "jdbc:mysql://localhost:3306/advjava";
	private static final String DB_USER = "nilesh";
	private static final String DB_PASSWORD = "nilesh";

	public static void main(String[] args) {
		System.out.println("Hello Maven!");
		String sql = "SELECT id, name FROM topics";
		try(Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
			try(PreparedStatement stmt = con.prepareStatement(sql)) {
				try(ResultSet rs = stmt.executeQuery()) {
					while(rs.next()) {
						int id = rs.getInt("id");
						String name = rs.getString("name");
						System.out.println(id + ", " + name);
					}
				} // rs.close()
			} // stmt.close()
		} // con.close() 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}







