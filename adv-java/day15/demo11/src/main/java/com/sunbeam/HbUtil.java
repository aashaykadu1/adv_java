package com.sunbeam;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HbUtil {
	private static SessionFactory sessionFactory = buildSessionFactory();
	private static StandardServiceRegistry serviceRegistry;
	
	private static SessionFactory buildSessionFactory() {
		// Hibernate 5 bootstrapping -- Lab time
		try {
			// Create ServiceRegistry using builder pattern
			serviceRegistry = new StandardServiceRegistryBuilder()
				.configure() // read hibernate.cfg.xml
				.build();
			// Create Metadata using builder pattern
			Metadata metadata = new MetadataSources(serviceRegistry)
				.getMetadataBuilder()
				.build();
			// Create SessionFactory
			return metadata.buildSessionFactory();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}
	
	private static SessionFactory buildSessionFactoryJava() {
		// Hibernate 5 bootstrapping -- Lab time
		try {
			// Create ServiceRegistry using builder pattern
			serviceRegistry = new StandardServiceRegistryBuilder()
				.applySetting("hibernate.connection.driver_class", "com.mysql.cj.jdbc.Driver")
				.applySetting("hibernate.connection.url", "jdbc:mysql://localhost:3306/advjava")
				.applySetting("hibernate.connection.username", "nilesh")
				.applySetting("hibernate.connection.password", "nilesh")
				.applySetting("hibernate.dialect", "org.hibernate.dialect.MySQL8Dialect")
				.applySetting("hibernate.show_sql", "true")
				.build();
			// Create Metadata using builder pattern
			Metadata metadata = new MetadataSources(serviceRegistry)
				.addAnnotatedClass(Book.class)
				//.addAnnotatedClass(Customer.class)
				.getMetadataBuilder()
				.build();
			// Create SessionFactory
			return metadata.buildSessionFactory();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	public static void shutdown() {
		sessionFactory.close();
	}
}

