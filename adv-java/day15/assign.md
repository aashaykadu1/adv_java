### Hibernate - Assignment
* Tutorials Database -- "users" table
* Implement User entity class.
	```Java
	@Entity
	@Table(name="users")
	class User {
		// ...
		@Column(name = "reg_date")
		@Temporal(TemporalType.DATE)    // java.util.Date --> java.sql.Date while internal JDBC ops
		private Date regDate; // java.util.Date
		// ...
	}
	```
* Implement UserDaoImpl with folling functinalities.
	* User findById(int id);
	* List<User> findAll();
	* void update(User user);
	* void deleteById(int id);
	* void save(User user);
* Write a main class and test all methods one by one.

### Spring MVC - Assignment
* Add Tutorial --> Topics - Dropdown List
* In TutorialControllerImpl class
    ```Java
    @GetMapping("/add")
    public String newTutorial(Model model) {
        // write usual logic & model attributes (command, ...)
        List<Topic> topicList = tutorialService.findAllTopics();
        model.addAttribute("topicList", topicList);
        return "add"; // --> add.jsp
    }
    ```
* add.jsp
    ```JSP
    <sf:form ...>
    <%-- other form controls --%>
    <sf:select path="topicId">  -- topicId is field of Tutorial for topic selection.
        <sf:options items="${topicList}" itemValue="id" itemLabel="name"/>
    </sf:select>
    <%-- other form controls --%>
    </sf:form>
    ```
* Browser: Load Add page ---> View Page Source --> See Dropdown HTML generated.
* In TutorialControllerImpl class
    ```Java
    @PostMapping("/add")
    public String addTutorial(Tutorial tutorial, Model model) {
        // write usual logic
        tutorialService.saveTutorial(tutorial); // implement method in tutorialService & tutorialDao
        return "redirect:manage";
    }
    ```
