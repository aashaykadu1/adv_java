package com.sunbeam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThmvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThmvcApplication.class, args);
	}

}
