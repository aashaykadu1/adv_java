package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.BlogRating;

public interface BlogRatingDao extends JpaRepository<BlogRating, Integer> {
	BlogRating findById(int id);
	List<BlogRating> findByBlogId(int blogId);
	BlogRating findByBlogIdAndUserId(int blogId, int userId);
}

