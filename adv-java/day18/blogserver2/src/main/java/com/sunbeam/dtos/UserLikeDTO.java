package com.sunbeam.dtos;

public class UserLikeDTO {
	private int userId;
	private int type;
	public UserLikeDTO() {
	}
	public UserLikeDTO(int userId, int type) {
		this.userId = userId;
		this.type = type;
	}

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return String.format("UserLikeDTO [userId=%s, type=%s]", userId, type);
	}
}
