package com.sunbeam.services;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.BlogAttachmentDao;
import com.sunbeam.daos.BlogCommentDao;
import com.sunbeam.daos.BlogDao;
import com.sunbeam.daos.BlogLikeDao;
import com.sunbeam.daos.BlogRatingDao;
import com.sunbeam.daos.UserDao;
import com.sunbeam.dtos.BlogAttachmentFormDTO;
import com.sunbeam.dtos.BlogCommentDTO;
import com.sunbeam.dtos.BlogDTO;
import com.sunbeam.dtos.BlogRatingDTO;
import com.sunbeam.dtos.DtoEntityConverter;
import com.sunbeam.dtos.UserLikeDTO;
import com.sunbeam.dtos.UserRatingDTO;
import com.sunbeam.entities.Blog;
import com.sunbeam.entities.BlogAttachment;
import com.sunbeam.entities.BlogComment;
import com.sunbeam.entities.BlogLikeStatus;
import com.sunbeam.entities.BlogRating;
import com.sunbeam.entities.User;

@Transactional
@Service
public class BlogServiceImpl {
	@Autowired
	private UserDao userDao;
	@Autowired
	private BlogDao blogDao;
	@Autowired
	private BlogAttachmentDao attachmentDao;
	@Autowired
	private BlogCommentDao commentDao;
	@Autowired
	private BlogLikeDao likeDao;
	@Autowired
	private BlogRatingDao ratingDao;
	@Autowired
	private DtoEntityConverter converter;
	
	public List<BlogDTO> findAllBlogs() {
		List<Blog> blogList = blogDao.findAll();
		return blogList.stream()
			.map(blog -> converter.toBlogDTO(blog))
			.collect(Collectors.toList());
	}

	public List<BlogDTO> findBlogsByTitle(String title) {
		List<Blog> blogList = blogDao.findByTitleContaining(title);
		return blogList.stream()
			.map(blog -> converter.toBlogDTO(blog))
			.collect(Collectors.toList());
	}
	
	public List<BlogDTO> findBlogsByTag(String tag) {
		List<Blog> blogList = blogDao.findByTagsContaining(tag);
		return blogList.stream()
			.map(blog -> converter.toBlogDTO(blog))
			.collect(Collectors.toList());
	}

	public BlogDTO findBlogById(int blogId) {
		Blog blog = blogDao.findById(blogId);
		return converter.toBlogDTO(blog);
	}

	public Map<String, Object> addBlog(BlogDTO blogDto) {
		Blog blog = converter.toBlogEntity(blogDto);
		blog = blogDao.save(blog);
		return Collections.singletonMap("insertedId", blog.getId());
	}
	
	public Map<String, Object> addBlogComment(int blogId, BlogCommentDTO commentDto) {
		commentDto.setBlogId(blogId);
		BlogComment comment = converter.toCommentEntity(commentDto);
		comment = commentDao.save(comment);
		return Collections.singletonMap("insertedId", comment.getId());
	}

	public Map<String, Object> editBlog(int blogId, BlogDTO blogDto) {
		if(blogDao.existsById(blogId)) {
			blogDto.setId(blogId);
			Blog blog = converter.toBlogEntity(blogDto);
			blog.setUser(userDao.findById(blogDto.getUserId()));
			blog = blogDao.save(blog);
			return Collections.singletonMap("changedRows", 1);
		}
		return Collections.singletonMap("changedRows", 0);
	}

	public Map<String, Object> deleteBlog(int blogId) {
		if(blogDao.existsById(blogId)) {
			blogDao.deleteById(blogId);
			return Collections.singletonMap("affectedRows", 1);
		}
		return Collections.singletonMap("affectedRows", 0);
	}

	public Map<String, Object> saveBlogLike(int blogId, UserLikeDTO userLike) {
		BlogLikeStatus likeStatus = likeDao.findByBlogIdAndUserId(blogId, userLike.getUserId());
		if(likeStatus != null) {
			if(likeStatus.getType() != userLike.getType()) {
				likeStatus.setType(userLike.getType());
				likeDao.save(likeStatus);
				return Collections.singletonMap("changedRows", 1);
			}
			return Collections.singletonMap("affectedRows", 0);
		}
		else {
			likeStatus = new BlogLikeStatus(0, userLike.getType(), new Date());
			likeStatus.setUser(new User(userLike.getUserId()));
			likeStatus.setBlog(new Blog(blogId));
			likeStatus = likeDao.save(likeStatus);
			return Collections.singletonMap("insertedId", likeStatus.getId());
		}
	}

	public Map<String, Object> saveBlogRating(int blogId, UserRatingDTO userRating) {
		BlogRating rating = ratingDao.findByBlogIdAndUserId(blogId, userRating.getUserId());
		if(rating != null) {
			if(rating.getRating() != userRating.getRating()) {
				rating.setRating(userRating.getRating());
				ratingDao.save(rating);
				return Collections.singletonMap("changedRows", 1);
			}
			return Collections.singletonMap("affectedRows", 0);
		}
		else {
			rating = new BlogRating(0, userRating.getRating(), new Date());
			rating.setUser(new User(userRating.getUserId()));
			rating.setBlog(new Blog(blogId));
			rating = ratingDao.save(rating);
			return Collections.singletonMap("insertedId", rating.getId());
		}
	}
	
	public Map<String,Object> getBlogAvgRating(int blogId) {
		Float rating = blogDao.getAvgRating(blogId);
		return Collections.singletonMap("avg", rating);
	}

	public List<BlogRatingDTO> findBlogRatingsByBlogId(int blogId) {
		List<BlogRating> list = ratingDao.findByBlogId(blogId);
		return list.stream().map(r -> converter.toRatingDTO(r)).collect(Collectors.toList());
	}
	
	public List<BlogCommentDTO> findBlogCommentsByBlogId(int blogId) {
		List<BlogComment> list = commentDao.findByBlogId(blogId);
		return list.stream().map(bc -> converter.toCommentDTO(bc)).collect(Collectors.toList());
	}

	public Map<String, Object> addBlogAttachment(int blogId, BlogAttachmentFormDTO attachmentDto) {
		attachmentDto.setBlogId(blogId);
		BlogAttachment attachment = converter.toAttachmentEntity(attachmentDto);
		attachment = attachmentDao.save(attachment);
		return Collections.singletonMap("insertedId", attachment.getId());
	}
}
