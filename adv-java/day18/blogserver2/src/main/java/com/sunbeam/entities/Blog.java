package com.sunbeam.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Blog {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "userId")
	private User user;
	private String title;
	private String details; 
	private String tags;
	private int state;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(insertable = false)
	private Date createdTimestamp;
	@OneToMany(mappedBy = "blog")
	private List<BlogAttachment> attachList;
	@OneToMany(mappedBy = "blog")
	private List<BlogRating> ratingList;
	@OneToMany(mappedBy = "blog")
	private List<BlogLikeStatus> likesList;
	@OneToMany(mappedBy = "blog")
	private List<BlogComment> commentList;
	
	public Blog() {
	}
	
	public Blog(int id, String title, String details, String tags, int state, Date createdTimestamp) {
		this.id = id;
		this.title = title;
		this.details = details;
		this.tags = tags;
		this.state = state;
		this.createdTimestamp = createdTimestamp;
	}

	public Blog(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public List<BlogAttachment> getAttachList() {
		return attachList;
	}

	public void setAttachList(List<BlogAttachment> attachList) {
		this.attachList = attachList;
	}

	public List<BlogRating> getRatingList() {
		return ratingList;
	}

	public void setRatingList(List<BlogRating> ratingList) {
		this.ratingList = ratingList;
	}

	public List<BlogLikeStatus> getLikesList() {
		return likesList;
	}

	public void setLikesList(List<BlogLikeStatus> likesList) {
		this.likesList = likesList;
	}

	public List<BlogComment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<BlogComment> commentList) {
		this.commentList = commentList;
	}

	public void addAttachment(BlogAttachment attachment) {
		if(this.attachList == null)
			this.attachList = new ArrayList<>();
		int index = this.attachList.indexOf(attachment);
		if(index == -1)
			this.attachList.add(attachment);
		else
			this.attachList.set(index, attachment);
		attachment.setBlog(this);
	}
	
	public void addRating(BlogRating rating) {
		if(this.ratingList == null)
			this.ratingList = new ArrayList<>();
		int index = this.ratingList.indexOf(rating);
		if(index == -1)
			this.ratingList.add(rating);
		else
			this.ratingList.set(index, rating);
		rating.setBlog(this);
	}

	public void addLike(BlogLikeStatus like) {
		if(this.ratingList == null)
			this.likesList = new ArrayList<>();
		int index = this.likesList.indexOf(like);
		if(index == -1)
			this.likesList.add(like);
		else
			this.likesList.set(index, like);
		like.setBlog(this);
	}
	
	public void addComment(BlogComment comment) {
		// TODO
	}

	@Override
	public String toString() {
		return String.format("Blog [id=%s, user=%s, title=%s, details=%s, tags=%s, state=%s, createdTimestamp=%s]",
				id, (user == null ? "-" : user.getId()), title, details, tags, state, createdTimestamp);
	}

}
