package com.sunbeam.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dtos.BlogAttachmentFormDTO;
import com.sunbeam.dtos.BlogCommentDTO;
import com.sunbeam.dtos.BlogDTO;
import com.sunbeam.dtos.BlogRatingDTO;
import com.sunbeam.dtos.Response;
import com.sunbeam.dtos.UserLikeDTO;
import com.sunbeam.dtos.UserRatingDTO;
import com.sunbeam.entities.BlogAttachment;
import com.sunbeam.services.BlogServiceImpl;

@CrossOrigin(origins = "*")
@RestController
public class BlogControllerImpl {
	@Autowired
	private BlogServiceImpl blogService;
	
	@GetMapping("/blog/search")
	public ResponseEntity<?> findBlogs(
			@RequestParam(name = "title", defaultValue = "") String title,
			@RequestParam(name = "tag", defaultValue = "") String tag) {
		List<BlogDTO> result = new ArrayList<>();
		if(!title.isEmpty())
			result = blogService.findBlogsByTitle(title);
		else if(!tag.isEmpty())
			result = blogService.findBlogsByTag(tag);
		else
			result = blogService.findAllBlogs();
		return Response.success(result);
	}

	@GetMapping("/blog/details/{id}")
	public ResponseEntity<?> findBlogById(@PathVariable("id") int id) {
		BlogDTO result = blogService.findBlogById(id);
		return Response.success(result);
	}
	
	@PostMapping("/blog")
	public ResponseEntity<?> addBlog(@RequestBody BlogDTO blogDto) {
		System.out.println("Inserting: " + blogDto);
		Map<String, Object> result = blogService.addBlog(blogDto);
		return Response.success(result);
	}

	@PutMapping("/blog/{id}")
	public ResponseEntity<?> editBlog(@PathVariable("id") int id, @RequestBody BlogDTO blogDto) {
		Map<String, Object> result = blogService.editBlog(id, blogDto);
		return Response.success(result);
	}
	
	@DeleteMapping("/blog/{id}")
	public ResponseEntity<?> deleteBlog(@PathVariable("id") int id) {
		Map<String, Object> result = blogService.deleteBlog(id);
		return Response.success(result);
	}
	
	@PatchMapping("/blog/{id}/toggle-like")
	public ResponseEntity<?> toggleBlogLike(@PathVariable("id") int id, @RequestBody UserLikeDTO userLike) {
		Map<String, Object> result = blogService.saveBlogLike(id, userLike);
		return Response.success(result);
	}
	
	@PostMapping("/ratings/{id}")
	public ResponseEntity<?> saveRating(@PathVariable("id") int id, @RequestBody UserRatingDTO userRating) {
		Map<String, Object> result = blogService.saveBlogRating(id, userRating);
		return Response.success(result);
	}
	
	@PostMapping("/comment/{id}")
	public ResponseEntity<?> saveComment(@PathVariable("id") int blogId, @RequestBody BlogCommentDTO comment) {
		Map<String, Object> result = blogService.addBlogComment(blogId, comment);
		return Response.success(result);
	}
	
	@PostMapping("/attachment/{id}")
	public ResponseEntity<?> saveAttachment(@PathVariable("id") int blogId, BlogAttachmentFormDTO attachment) {
		Map<String, Object> result = blogService.addBlogAttachment(blogId, attachment);
		return Response.success(result);
	}
	
	@GetMapping("/rating/{id}/aggregate")
	public ResponseEntity<?> getAvgRating(@PathVariable("id") int blogId) {
		Map<String, Object> result = blogService.getBlogAvgRating(blogId);
		return Response.success(result);
	}
	
	@GetMapping("/rating/{id}")
	public ResponseEntity<?> findBlogRatings(@PathVariable("id") int blogId) {
		List<BlogRatingDTO> result = blogService.findBlogRatingsByBlogId(blogId);
		return Response.success(result);
	}

	@GetMapping("/comment/{id}")
	public ResponseEntity<?> findBlogComments(@PathVariable("id") int blogId) {
		List<BlogCommentDTO> result = blogService.findBlogCommentsByBlogId(blogId);
		return Response.success(result);
	}
}
