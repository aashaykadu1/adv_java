package com.sunbeam;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.UserDao;
import com.sunbeam.entities.User;

@SpringBootTest
class UserDaoTests {
	@Autowired
	private UserDao userDao;
	
	@Test
	void testFindAll() {
		List<User> list = userDao.findAll();
		list.forEach(System.out::println);
		//assertThat(list).isNotEmpty();
	}

	@Test
	void testFindById() {
		User user = userDao.findById(1);
		System.out.println("Found by Id: " + user);
	}

	@Test
	void testFindByEmail() {
		User user = userDao.findByEmail("nilesh@gmail.com");
		System.out.println("Found by Id: " + user);
	}
	
	@Rollback(false)
	@Transactional
	@Test
	void testSave() {
		User user = new User(0, "Nitin", "Kudale", "$2a$10$BQKnUsC3gyGQR6qfowPnguWEQdr5djDbS4gZQLW8YpWOx6aTfSzMC", "nitin@gmail.com", null, 1, new Date());
		userDao.save(user);
	}
}
