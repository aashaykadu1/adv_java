package com.sunbeam.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.entities.User;
import com.sunbeam.services.UserServiceImpl;

@RequestMapping("/v2/users")
@RestController // @Controller + @ResponseBody. 
public class UserControllerImplV2 {
	@Autowired
	private UserServiceImpl userService;

	@GetMapping("") // /v2/users + ""
	public List<User> findAll() {
		List<User> result = userService.findAllUsers();
		return result;
	}
	
	@GetMapping("/{id}") // /v2/users + "/{id}"
	public Object findById(@PathVariable("id") int id) {
		User result = userService.findUserById(id);
		if(result == null)
			return "User not found";
		return result;
	}
	
	@PostMapping("")
	public User save(@RequestBody User user) {
		User result = userService.save(user);
		return result;
	}
	
	@PutMapping("/{id}")
	public User update(@PathVariable("id") int id, @RequestBody User user) {
		user.setId(id);
		User result = userService.save(user);
		return result;
	}	
	
	@DeleteMapping("/{id}")
	public String deleteById(@PathVariable("id") int id) {
		int count = userService.deleteUserById(id);
		return "Records Deleted: " + count;
	}
}

