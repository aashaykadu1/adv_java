package com.sunbeam.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.UserDao;
import com.sunbeam.entities.User;

@Transactional
@Service
public class UserServiceImpl {
	@Autowired
	private UserDao userDao;

	public List<User> findAllUsers() {
		return userDao.findAll();
	}
	
	public User findUserById(int userId) {
		return userDao.findById(userId);
	}
	
	public User findUserByEmail(String email) {
		return userDao.findByEmail(email);
	}
	
	public User save(User user) {
		return userDao.save(user);
	}
	
	public int deleteUserById(int userId) {
		if(userDao.existsById(userId)) {
			userDao.deleteById(userId);
			return 1;
		}
		return 0;
	}
}
