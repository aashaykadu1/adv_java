package com.sunbeam.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sunbeam.entities.User;
import com.sunbeam.services.UserServiceImpl;

@Controller
public class UserControllerImplV1 {
	@Autowired
	private UserServiceImpl userService;

	@GetMapping("/v1/users")
	public @ResponseBody List<User> findAll() {
		List<User> result = userService.findAllUsers();
		return result;
	}
	
	@GetMapping("/v1/users/{id}")
	public @ResponseBody Object findById(@PathVariable("id") int id) {
		User result = userService.findUserById(id);
		if(result == null)
			return "User not found";
		return result;
	}
	
	@PostMapping("/v1/users")
	public @ResponseBody User save(@RequestBody User user) {
		User result = userService.save(user);
		return result;
	}
	
	@PutMapping("/v1/users/{id}")
	public @ResponseBody User update(@PathVariable("id") int id, @RequestBody User user) {
		user.setId(id);
		User result = userService.save(user);
		return result;
	}	
	
	@DeleteMapping("/v1/users/{id}")
	public @ResponseBody String deleteById(@PathVariable("id") int id) {
		int count = userService.deleteUserById(id);
		return "Records Deleted: " + count;
	}
}


/*
 * JpaRepository --> save()
 * 		- Call INSERT if record not present
 * 		- Call UPDATE if record present
 * */





