package com.sunbeam.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.entities.User;
import com.sunbeam.services.UserServiceImpl;

@RequestMapping("/v3/users")
@RestController // @Controller + @ResponseBody. 
public class UserControllerImplV3 {
	@Autowired
	private UserServiceImpl userService;

	@GetMapping("") 
	public ResponseEntity<?> findAll() {
		List<User> result = userService.findAllUsers();
		return ResponseEntity.ok(result);	// http status = 200, body = result
	}
	
	@GetMapping("/{id}") 
	public ResponseEntity<?> findById(@PathVariable("id") int id) {
		User result = userService.findUserById(id);
		if(result == null)
			return ResponseEntity.notFound().build(); // http status = 404
		return ResponseEntity.ok(result);	// http status = 200, body = result
	}
	
	@PostMapping("")
	public ResponseEntity<?> save(@RequestBody User user) {
		User result = userService.save(user);
		return ResponseEntity.ok(result);	// http status = 200, body = result
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody User user) {
		user.setId(id);
		User result = userService.save(user);
		return ResponseEntity.ok(result);	// http status = 200, body = result
	}	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteById(@PathVariable("id") int id) {
		int count = userService.deleteUserById(id);
		if(count == 0)
			return ResponseEntity.notFound().build(); // http status = 404
		return ResponseEntity.ok("Record Deleted");	// http status = 200, body = "Record Deleted"
	}
}

