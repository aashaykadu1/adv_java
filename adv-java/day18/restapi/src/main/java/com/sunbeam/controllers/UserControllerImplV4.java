package com.sunbeam.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.entities.User;
import com.sunbeam.services.UserServiceImpl;

@RequestMapping("/v4/users")
@RestController // @Controller + @ResponseBody. 
public class UserControllerImplV4 {
	@Autowired
	private UserServiceImpl userService;

	@GetMapping("") 
	public ResponseEntity<?> findAll() {
		try {
			List<User> result = userService.findAllUsers();
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	@GetMapping("/{id}") 
	public ResponseEntity<?> findById(@PathVariable("id") int id) {
		try {
			User result = userService.findUserById(id);
			if (result == null)
				return Response.error("Not found");
			return Response.success(result); // http status = 200, body = result
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	@PostMapping("")
	public ResponseEntity<?> save(@RequestBody User user) {
		try {
			User result = userService.save(user);
			return Response.success(result); // http status = 200, body = result
		} catch (Exception e) {
			return Response.error(e.getMessage()); 
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody User user) {
		try {
			user.setId(id);
			User result = userService.save(user);
			return Response.success(result); // http status = 200, body = result
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteById(@PathVariable("id") int id) {
		try {
			int count = userService.deleteUserById(id);
			if (count == 0)
				return Response.status(HttpStatus.NOT_FOUND); // http status = 404
			return Response.success("Deleted Count: " + count); // http status = 200, body = "Record Deleted"
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
}

/* data = result and status = success
 * 
 * Method1: Create new Pojo class for MyResponse -- String status, Object data, String msg;
 * 		resp = new MyResponse("success", result, null);
 * 		return ResponseEntitiy.ok(resp);
 * 
 * Method2: Create new HashMap.
 * 		resp = new HashMap<String,Object>();
 * 		resp.put("status", "success");
 * 		resp.put("data", result);
 * 		return ResponseEntitiy.ok(resp);
 * 
 * */




