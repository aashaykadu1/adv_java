package com.sunbeam.entities;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="blog_attachment")
public class BlogAttachment {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;
	@ManyToOne
	@JoinColumn(name = "blogId")
	private Blog blog;
	@Lob
	private byte[] data;
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdTimestamp;
	
	public BlogAttachment() {
	}

	public BlogAttachment(int id, byte[] data, Date createdTimestamp) {
		this.id = id;
		this.data = data;
		this.createdTimestamp = createdTimestamp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Blog getBlog() {
		return blog;
	}

	public void setBlog(Blog blog) {
		this.blog = blog;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	@Override
	public String toString() {
		return String.format("BlogAttachment [id=%s, blog=%s, data=%s, createdTimestamp=%s]", id, (blog == null ? "-" : blog.getId()),
				(data == null? 0 : data.length), createdTimestamp);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof BlogAttachment))
			return false;
		BlogAttachment other = (BlogAttachment) obj;
		return id == other.id;
	}
}
