package com.sunbeam.entities;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="blog_like_status")
public class BlogLikeStatus {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;
	@ManyToOne
	@JoinColumn(name = "userId")
	private User user;
	@ManyToOne
	@JoinColumn(name = "blogId")
	private Blog blog;
	private int type; // 0: dislike, 1: like
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdTimestamp;

	public BlogLikeStatus() {
	}

	public BlogLikeStatus(int id, int type, Date createdTimestamp) {
		this.id = id;
		this.type = type;
		this.createdTimestamp = createdTimestamp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Blog getBlog() {
		return blog;
	}

	public void setBlog(Blog blog) {
		this.blog = blog;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	@Override
	public String toString() {
		return String.format("BlogComment [id=%s, user=%s, blog=%s, type=%s, createdTimestamp=%s]", id, user==null ? "-" : user.getId(), blog == null ? "-" : blog.getId(),
				(type == 1 ? "like" : "dislike"), createdTimestamp);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof BlogLikeStatus))
			return false;
		BlogLikeStatus other = (BlogLikeStatus) obj;
		return id == other.id;
	}
}
