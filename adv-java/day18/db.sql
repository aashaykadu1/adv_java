use blogs_db;

drop table if exists user;
drop table if exists blog;
drop table if exists blog_attachment;
drop table if exists blog_comment;
drop table if exists blog_rating;
drop table if exists blog_like_status;

create table user (
  id integer primary key auto_increment,
  firstName varchar(50),
  lastName varchar(50),
  password varchar(600),
  email varchar(50),
  birthDate date,
  addressLine1 varchar(50),
  addressLine2 varchar(50),
  city varchar(50),
  state varchar(50),
  country varchar(50),
  postalCode integer,
  phone varchar(50),
  gender int(1), /* 0: female, 1: male */
  profileImage varchar(50),
  isActive int(1) default 1,
  createdTimestamp timestamp default CURRENT_TIMESTAMP
);

insert into user(firstName, lastName, email, password) values('Amit', 'Kulkarni', 'amit@test.com', '$2a$10$OA1LU95M8mGh3CtmDhnU5.m4t5sVxbQxoH9vNQG8ZQSUpvZVFOqDW');
insert into user(firstName, lastName, email, password) values('Nilesh', 'Ghule', 'nilesh@gmail.com', '$2a$10$ITuJBvTEcPgsX5WpyjYJ.uFZg8bhpIRmadA5CHAdjOZb6DybYBsdC');
insert into user(firstName, lastName, email, password) values('Yogesh', 'Kolhe', 'yogesh@gmail.com', '$2a$10$aUe1di.yTvzFCzBljpASHe3iC/WX150X.G0keL9JIGSb5G/q29.VW');
insert into user(firstName, lastName, email, password) values('Shubham', 'Borle', 'shubham@gmail.com', '$2a$10$boSnPS4VQIXL7YLn8nbOSOM9GLeOCf0mvEt7eTDMh4pDTmqAVd54W');
insert into user(firstName, lastName, email, password) values('Rohan', 'Paramane', 'rohan@gmail.com', '$2a$10$3wL6//YsdztfZSh9pmnhNO8mCDD5fcgz9MWyk7ZL0rJJYrfSAGICS');
insert into user(firstName, lastName, email, password) values('Blogs', 'Admin', 'admin@blogs.com', '$2a$10$iCRz2HoYKEdidHXzV3bczOF5LV2OxOoDxgVy1rvyckrID904fIIPi');

create table blog (
  id integer primary key auto_increment,
  userId integer,
  title varchar(100),
  details varchar(2048),
  tags varchar(500),
  state int(1) default 0,   /* 0: draft, 1: private, 2: public */
  createdTimestamp timestamp default CURRENT_TIMESTAMP
);

insert into blog(userId, title, details, tags, state) values
(1, 'React Intro', 'React is a free and open-source front-end JavaScript library for building user interfaces based on UI components. It is maintained by Meta and a community of individual developers and companies. React can be used as a base in the development of single-page or mobile applications.', 'js,react', 2);

insert into blog(userId, title, details, tags, state) values
(2, 'Spring Boot Intro', 'Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run". We take an opinionated view of the Spring platform and third-party libraries so you can get started with minimum fuss. Most Spring Boot applications need minimal Spring configuration.', 'java,spring,boot', 2);

insert into blog(userId, title, details, tags, state) values
(1, 'DevOps Intro', 'DevOps is a set of practices that combines software development and IT operations. It aims to shorten the systems development life cycle and provide continuous delivery with high software quality. DevOps is complementary with Agile software development; several DevOps aspects came from the Agile methodology.', 'devops,agile', 2);

insert into blog(userId, title, details, tags, state) values
(2, 'RDBMS Intro', 'A relational database is a digital database based on the relational model of data, as proposed by E. F. Codd in 1970. A system used to maintain relational databases is a relational database management system (RDBMS). Many relational database systems have an option of using the SQL (Structured Query Language) for querying and maintaining the database.', 'dbms,rdbms', 2);

create table blog_attachment(
  id integer primary key auto_increment,
  data blob,
  blogId integer,
  createdTimestamp timestamp default CURRENT_TIMESTAMP
);

create table blog_comment(
  id integer primary key auto_increment,
  userId integer,
  blogId integer,
  comment varchar(1024),
  createdTimestamp timestamp default CURRENT_TIMESTAMP
);

insert into blog_comment(userId, blogId, comment) values
(3, 1, 'Thanks for sharing this.');
insert into blog_comment(userId, blogId, comment) values
(4, 1, 'This is very nice introduction.');
insert into blog_comment(userId, blogId, comment) values
(5, 1, 'Amazing blog.');
insert into blog_comment(userId, blogId, comment) values
(3, 2, 'I loved this content.');
insert into blog_comment(userId, blogId, comment) values
(4, 2, 'This is precise information.');
insert into blog_comment(userId, blogId, comment) values
(5, 3, 'This is cool.');

create table blog_rating(
  id integer primary key auto_increment,
  userId integer,
  blogId integer,
  rating float,
  createdTimestamp timestamp default CURRENT_TIMESTAMP
);

insert into blog_rating(userId, blogId, rating) values (3, 1, 4);
insert into blog_rating(userId, blogId, rating) values (3, 2, 5);
insert into blog_rating(userId, blogId, rating) values (3, 3, 3);
insert into blog_rating(userId, blogId, rating) values (3, 4, 5);
insert into blog_rating(userId, blogId, rating) values (4, 1, 3);
insert into blog_rating(userId, blogId, rating) values (4, 2, 2);
insert into blog_rating(userId, blogId, rating) values (4, 3, 3);
insert into blog_rating(userId, blogId, rating) values (5, 1, 4);
insert into blog_rating(userId, blogId, rating) values (5, 2, 5);
insert into blog_rating(userId, blogId, rating) values (5, 3, 5);
insert into blog_rating(userId, blogId, rating) values (5, 4, 4);

create table blog_like_status(
  id integer primary key auto_increment,
  userId integer,
  blogId integer,
  type int(1),
  createdTimestamp timestamp default CURRENT_TIMESTAMP
);

insert into blog_like_status(userId, blogId, type) values (3, 1, 1);
insert into blog_like_status(userId, blogId, type) values (3, 2, 1);
insert into blog_like_status(userId, blogId, type) values (3, 3, 1);
insert into blog_like_status(userId, blogId, type) values (3, 4, 1);
insert into blog_like_status(userId, blogId, type) values (4, 1, 1);
insert into blog_like_status(userId, blogId, type) values (4, 2, 1);
insert into blog_like_status(userId, blogId, type) values (4, 3, 1);
insert into blog_like_status(userId, blogId, type) values (5, 1, 1);
insert into blog_like_status(userId, blogId, type) values (5, 2, 1);
insert into blog_like_status(userId, blogId, type) values (5, 3, 1);
insert into blog_like_status(userId, blogId, type) values (5, 4, 1);

commit;
