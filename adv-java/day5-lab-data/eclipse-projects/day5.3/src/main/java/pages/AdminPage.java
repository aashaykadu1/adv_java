package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pojos.Voter;

/**
 * Servlet implementation class LogoutPage
 */
@WebServlet("/admin_page")
public class AdminPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// set cont type
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			// get HS from WC
			HttpSession hs = request.getSession();
			// get user details
			Voter admin = (Voter) hs.getAttribute("clnt_details");
			if (admin != null)
				// welcome mesg for the admin
				pw.print("<h4>Welcome Admin , Hello , " + admin.getName()+"</h4>");
			else
				pw.print("<h5> No Cookies !!!!! , Session Tracking failed...</h5>");
	 
				//more jobs will be added later under admin login(eg : new candidate registration...)
			//add a link for admin's logout
			pw.print("<h5> <a href='logout'>Log Me Out</a></h5>");


		}
	}

}
