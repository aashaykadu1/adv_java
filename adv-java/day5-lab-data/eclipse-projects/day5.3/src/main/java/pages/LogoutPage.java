package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImpl;
import dao.VoterDaoImpl;
import pojos.Voter;

/**
 * Servlet implementation class LogoutPage
 */
@WebServlet("/logout")
public class LogoutPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// set cont type
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {

			// get user details from session scope
			// 1. get session
			HttpSession session = request.getSession();
			// get user details from HS
			// send "Visit Again" : login.html : in case admin had logged in
			Voter user = (Voter) session.getAttribute("clnt_details");

			if (user != null) {
				pw.print("<h5> User Details from logout page : " + user + "</h5>");
				// chk the role
				if (user.getRole().equals("admin"))
					pw.print("<h5><a href='login.html'>Visit Again</a></h5>");
				else {
					// chk if just now voted or already voted ?
					if (user.isStatus()) // => alrdy voted
						pw.print("<h5> You have already voted !!!!!!</h5>");
					else {// not yet voted
							// 1. get candidate id : from rq param
						int candidateId = Integer.parseInt(request.getParameter("candidate_id"));
						// incr votes for the candidate : CandidateDao
						// , change voting status : VoterDao
						// get daos from session scope
						CandidateDaoImpl candidateDao = (CandidateDaoImpl) session.getAttribute("candidate_dao");
						VoterDaoImpl voterDao = (VoterDaoImpl) session.getAttribute("voter_dao");
						// invoke dao's methods
						System.out.println(candidateDao.incrementCandidateVotes(candidateId));
						pw.print("<h4>" + voterDao.updateVotingStatus(user.getId()) + "</h4>");

					}
				}

			} else
				pw.print("<h5> No session Tracking : no cookies!!!!!!</h5>");
			// invalidate HttpSession
			session.invalidate();
			pw.print("<h5> You have logged out successfully.....</h5>");

		} catch (Exception e) {
			throw new ServletException("err in do-get of " + getClass(), e);
		}
	}

}
