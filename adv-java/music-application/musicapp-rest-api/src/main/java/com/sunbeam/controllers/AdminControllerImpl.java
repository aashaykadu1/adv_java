package com.sunbeam.controllers;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.daos.UserDao;
import com.sunbeam.dtos.GaanaResponse;
import com.sunbeam.util.DbInit;

@CrossOrigin
@RequestMapping("/admin")
@RestController
public class AdminControllerImpl {
	@Autowired
	private DbInit dbInit;
	@Autowired
	private UserDao userDao;
	
	@GetMapping("/init/sampledb")
	public ResponseEntity<GaanaResponse> initSampleDb() {
		if(!userDao.findAll().isEmpty())
			throw new RuntimeException("Database already initialized.");
		dbInit.initDatabase();
		HashMap<String, Object> result = new HashMap<>();
		result.put("users", dbInit.getUserList().size());
		result.put("artists", dbInit.getArtistList().size());
		result.put("albums", dbInit.getAlbumList().size());
		result.put("songs", dbInit.getSongList().size());
		return GaanaResponse.success(result);
	}

	@ExceptionHandler
	public ResponseEntity<GaanaResponse> handleException(Exception ex) {
		return GaanaResponse.error(ex.getMessage(), ex);
	}
}
