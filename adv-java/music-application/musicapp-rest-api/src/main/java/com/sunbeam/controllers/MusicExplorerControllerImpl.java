package com.sunbeam.controllers;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dtos.AlbumDTO;
import com.sunbeam.dtos.ArtistDTO;
import com.sunbeam.dtos.DtoEntityConverterImpl;
import com.sunbeam.dtos.GaanaResponse;
import com.sunbeam.dtos.SongDTO;
import com.sunbeam.entities.Album;
import com.sunbeam.entities.Artist;
import com.sunbeam.entities.Song;
import com.sunbeam.services.MusicServiceImpl;

@CrossOrigin
@RequestMapping({"/user", "/admin"})
@RestController
public class MusicExplorerControllerImpl {
	@Autowired
	private MusicServiceImpl musicService;
	@Autowired
	private DtoEntityConverterImpl dtoEntityConverter;
		
	@GetMapping("/artists")
	public ResponseEntity<GaanaResponse> artistList() {
		List<Artist> list = musicService.findAllArtist();
		Stream<ArtistDTO> result = list.stream().map(ar -> dtoEntityConverter.artistEntityToDto(ar));
		return GaanaResponse.success(result);
	}
	
	@GetMapping("/artists/{id}")
	public ResponseEntity<GaanaResponse> artistById(@PathVariable("id") int artistId) {
		Artist artist = musicService.findArtistById(artistId);
		ArtistDTO result = dtoEntityConverter.artistEntityToDto(artist);
		return GaanaResponse.success(result);
	}
	
	@GetMapping("/artists/{id}/albums")
	public ResponseEntity<GaanaResponse> artistAlbumList(@PathVariable("id") int artistId) {
		List<Album> list = musicService.findArtistAlbums(artistId);
		Stream<AlbumDTO> result = list.stream().map(al -> dtoEntityConverter.albumEntityToDto(al));
		return GaanaResponse.success(result);
	}
	
	@GetMapping("/artists/{id}/songs")
	public ResponseEntity<GaanaResponse> artistSongList(@PathVariable("id") int artistId) {
		List<Song> list = musicService.findArtistSong(artistId);
		Stream<SongDTO> result = list.stream().map(s -> dtoEntityConverter.songEntityToDto(s,true));
		return GaanaResponse.success(result);
	}
	
	@GetMapping("/albums")
	public ResponseEntity<GaanaResponse> albumList(Model model) {
		List<Album> list = musicService.findAllAlbum();
		Stream<AlbumDTO> result = list.stream().map(al -> dtoEntityConverter.albumEntityToDto(al));
		return GaanaResponse.success(result);
	}
	
	@GetMapping("/albums/{id}")
	public ResponseEntity<GaanaResponse> albumsById(@PathVariable("id") int albumId) {
		Album album = musicService.findAlbumById(albumId);
		AlbumDTO result = dtoEntityConverter.albumEntityToDto(album);
		return GaanaResponse.success(result);
	}
	
	@GetMapping("/albums/{id}/songs")
	public ResponseEntity<GaanaResponse> albumSongList(@PathVariable("id") int albumId) {
		List<Song> list = musicService.findAlbumSong(albumId);
		Stream<SongDTO> result = list.stream().map(s -> dtoEntityConverter.songEntityToDto(s,true));
		return GaanaResponse.success(result);
	}
	
	@GetMapping("/songs")
	public ResponseEntity<GaanaResponse> songList() {
		List<Song> list = musicService.findAllSong();
		Stream<SongDTO> result = list.stream().map(s -> dtoEntityConverter.songEntityToDto(s,true));
		return GaanaResponse.success(result);
	}
	
	@GetMapping("/songs/pages")
	public ResponseEntity<GaanaResponse> songPages(Pageable pageable) {
		Page<Song> list = musicService.findAllSong(pageable);
		Stream<SongDTO> result = list.stream().map(s -> dtoEntityConverter.songEntityToDto(s,true));
		return GaanaResponse.success(result);
	}
	
	@GetMapping("/songs/{id}")
	public ResponseEntity<GaanaResponse> songsById(@PathVariable("id") int songId) {
		Song song = musicService.findSongById(songId);
		SongDTO result = dtoEntityConverter.songEntityToDto(song, true);
		return GaanaResponse.success(result);
	}
	
	@GetMapping("/songs/title/{title}") 
	public ResponseEntity<GaanaResponse> searchSong(@PathVariable("title") String title) {
		List<Song> list = musicService.findSongByLikeTitle(title);
		Stream<SongDTO> result = list.stream().map(s -> dtoEntityConverter.songEntityToDto(s,true));
		return GaanaResponse.success(result);
	}
}
