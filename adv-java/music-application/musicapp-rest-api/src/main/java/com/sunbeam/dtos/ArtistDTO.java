package com.sunbeam.dtos;

public class ArtistDTO {
	private int id;
	private String firstName;
	private String lastName;
	private String thumbnail;
	private String type;
	
	public ArtistDTO() {
	}

	public ArtistDTO(int id, String firstName, String lastName, String thumbnail, String type) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.thumbnail = thumbnail;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "ArtistDTO [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", thumbnail=" + thumbnail + ", type=" + type + "]";
	}
}

