package com.sunbeam.dtos;

import org.springframework.web.multipart.MultipartFile;

public class AlbumFormDTO {
	private int id;
	private String title;
	private MultipartFile thumbnail;
	
	public AlbumFormDTO() {
	}

	public AlbumFormDTO(int id, String title, MultipartFile thumbnail) {
		this.id = id;
		this.title = title;
		this.thumbnail = thumbnail;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public MultipartFile getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(MultipartFile thumbnail) {
		this.thumbnail = thumbnail;
	}

	@Override
	public String toString() {
		return "AlbumFormDTO [id=" + id + ", title=" + title + ", thumbnail=" + thumbnail + "]";
	}
}

