package com.sunbeam.sb.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int id;
	@Column
	private String name;
	@Column(name="password")
	private String encPassword;
	@Column
	private Date birth;
	@Column
	private int enabled;
	@Embedded
	private Contact contact;
	@JsonManagedReference
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
	private Set<Role> authorities;
	
	public User() {
		this(0, "", "", new Date(), 1, new Contact("", "", ""));
	}
	
	public User(int id, String name, String encPassword, Date birth, int enabled, String mobile, String address, String email) {
		this(0, "", "", new Date(), 1, new Contact(mobile, address, email));
	}
	
	public User(int id, String name, String encPassword, Date birth, int enabled, Contact contact) {
		this.id = id;
		this.name = name;
		this.encPassword = encPassword;
		this.birth = birth;
		this.enabled = enabled;
		this.contact = contact;
		this.authorities = new HashSet<Role>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEncPassword() {
		return encPassword;
	}

	public void setEncPassword(String encPassword) {
		this.encPassword = encPassword;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public int getEnabled() {
		return enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Set<Role> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Role> authorities) {
		this.authorities = authorities;
	}
	
	public void addRole(Role role) {
		role.setUser(this);
		this.authorities.add(role);
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", encPassword=" + encPassword + ", birth=" + birth + ", enabled="
				+ enabled + ", contact=" + contact + ", authorities=" + authorities + "]";
	}

}
