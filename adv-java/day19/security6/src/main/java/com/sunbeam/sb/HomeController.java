package com.sunbeam.sb;

import java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
	@GetMapping("/")
	public String home() {
		return "Home";
	}


	@GetMapping("/user")
	public String user(Principal principal) {
		return "User " + principal.getName();
	}

	@GetMapping("/admin")
	public String amdin(Principal principal) {
		return "Admin " + principal.getName();
	}
}

