package com.sunbeam.sb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.sb.daos.UserDao;
import com.sunbeam.sb.entities.User;

@Transactional
@Service
public class ShopUserDetailsService implements UserDetailsService {
	@Autowired
	private UserDao userDao;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = userDao.findByContactEmail(email);
		if(user == null)
			throw new UsernameNotFoundException(email + " not found.");
		return new ShopUser(user);
	}
}
