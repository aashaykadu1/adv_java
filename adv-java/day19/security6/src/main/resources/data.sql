INSERT INTO users(id, name, password, mobile, address, email, birth, enabled) VALUES
(1, 'admin','$2a$10$Smvo81bOY4J1jg5vxosf2..VzXrhF2LNQERaLVT0M0p4VQKPLTLb2','1111111111','Online Book Shop','admin@onlinebooks.com', '1970-01-01', 1),
(2, 'nilesh', '$2a$10$9AdMO3jAMhSkz4ffLUYvneWVHc6SKlsY.I8EwJtez8odBTIO0PSlS', '9527331338', 'Katraj, Pune', 'nilesh@sunbeaminfo.com', '1983-09-28', 1),
(3, 'prashant','$2a$10$47D9E5QGX9KZed3YA.9Vrua3jLHGIQbLzvjNm8SYybBhUFxy42GgC','9881208114','Peth, Karad','prashant@sunbeaminfo.com', '1975-12-01', 1),
(4, 'nitin', '$2a$10$pZZHHEBCo7SPNVcwLWODlevBhTAUt9eiK/fYtA6DIIxqBpeZz791G', '9881208115', 'Padmavati, Pune', 'nitin@sunbeaminfo.com', '1974-01-23', 1);

INSERT INTO roles(id, user_id, role) VALUES
(1, 1, 'ROLE_ADMIN'),
(2, 2, 'ROLE_USER'),
(3, 3, 'ROLE_USER'),
(4, 4, 'ROLE_USER'),
(5, 2, 'ROLE_ADMIN');
