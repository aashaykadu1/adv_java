package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.sunbeam.entities.Blog;

public interface BlogDao extends JpaRepository<Blog, Integer> {
	Blog findById(int id);
	List<Blog> findByUserId(int id);
	@Modifying
	@Query("UPDATE Blog b SET b.state = ?2 WHERE b.id = ?1")
	int updateState(int id, int state);
	List<Blog> findByTitleContaining(String title);
	List<Blog> findByTagsContaining(String tag);
	@Query("SELECT SUM(bl.type) FROM BlogLikeStatus bl WHERE bl.blog.id = ?1")
	long getLikeCount(int id);
	@Query("SELECT AVG(br.rating) FROM BlogRating br WHERE br.blog.id = ?1")
	Float getAvgRating(int id);
}

