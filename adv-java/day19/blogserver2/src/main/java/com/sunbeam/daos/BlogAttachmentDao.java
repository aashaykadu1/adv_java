package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.BlogAttachment;

public interface BlogAttachmentDao extends JpaRepository<BlogAttachment, Integer> {
	BlogAttachment findById(int id);
	List<BlogAttachment> findByBlogId(int blogId);
}

