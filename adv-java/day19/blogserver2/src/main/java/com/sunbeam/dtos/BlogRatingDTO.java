package com.sunbeam.dtos;

import java.util.Date;

public class BlogRatingDTO {
	private Date createdTimestamp;
	private float rating;
	private int blogId;
	private int userId;
	private String firstName;
	private String lastName;
	private int id;
	
	public BlogRatingDTO() {
	}

	public BlogRatingDTO(Date createdTimestamp, float rating, int blogId, int userId, String firstName, String lastName,
			int id) {
		this.createdTimestamp = createdTimestamp;
		this.rating = rating;
		this.blogId = blogId;
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.id = id;
	}



	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimeStamp) {
		this.createdTimestamp = createdTimeStamp;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public int getBlogId() {
		return blogId;
	}

	public void setBlogId(int blogId) {
		this.blogId = blogId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return String.format(
				"BlogRatingDTO [createdTimestamp=%s, rating=%s, blogId=%s, userId=%s, firstName=%s, lastName=%s, id=%s]",
				createdTimestamp, rating, blogId, userId, firstName, lastName, id);
	}

}
