package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.BlogLikeStatus;

public interface BlogLikeDao extends JpaRepository<BlogLikeStatus, Integer> {
	BlogLikeStatus findById(int id);
	List<BlogLikeStatus> findByBlogId(int blogId);
	BlogLikeStatus findByBlogIdAndUserId(int blogId, int userId);
}

