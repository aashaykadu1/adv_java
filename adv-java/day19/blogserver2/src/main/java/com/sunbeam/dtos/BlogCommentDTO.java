package com.sunbeam.dtos;

import java.util.Date;

public class BlogCommentDTO {
	int id;
	private Date createdTimestamp;
	private String comment;
	private int blogId;
	private int userId;
	private String firstName;
	private String lastName;
	
	public BlogCommentDTO() {
	}

	public BlogCommentDTO(int id, Date createdTimestamp, String comment, int blogId, int userId, String firstName,
			String lastName) {
		this.id = id;
		this.createdTimestamp = createdTimestamp;
		this.comment = comment;
		this.blogId = blogId;
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimeStamp) {
		this.createdTimestamp = createdTimeStamp;
	}

	public int getBlogId() {
		return blogId;
	}

	public void setBlogId(int blogId) {
		this.blogId = blogId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return String.format(
				"RatingDTO [createdTimeStamp=%s, comment=%s, blogId=%s, userId=%s, firstName=%s, lastName=%s]",
				createdTimestamp, comment, blogId, userId, firstName, lastName);
	}
	
}
