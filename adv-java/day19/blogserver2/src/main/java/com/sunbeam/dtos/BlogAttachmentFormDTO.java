package com.sunbeam.dtos;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class BlogAttachmentFormDTO {
	int id;
	private Date createdTimestamp;
	private MultipartFile dataFile;
	private int blogId;
	
	public BlogAttachmentFormDTO() {
	}

	public BlogAttachmentFormDTO(int id, Date createdTimestamp, MultipartFile dataFile, int blogId) {
		this.id = id;
		this.createdTimestamp = createdTimestamp;
		this.dataFile = dataFile;
		this.blogId = blogId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public MultipartFile getDataFile() {
		return dataFile;
	}

	public void setDataFile(MultipartFile dataFile) {
		this.dataFile = dataFile;
	}

	public int getBlogId() {
		return blogId;
	}

	public void setBlogId(int blogId) {
		this.blogId = blogId;
	}

	@Override
	public String toString() {
		return String.format("BlogAttachmentFormDTO [id=%s, createdTimestamp=%s, dataFile=%s, blogId=%s]", id,
				createdTimestamp, dataFile, blogId);
	}
	
}
