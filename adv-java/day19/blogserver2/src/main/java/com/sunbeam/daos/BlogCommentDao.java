package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.BlogComment;

public interface BlogCommentDao extends JpaRepository<BlogComment, Integer> {
	BlogComment findById(int id);
	List<BlogComment> findByBlogId(int blogId);
}

