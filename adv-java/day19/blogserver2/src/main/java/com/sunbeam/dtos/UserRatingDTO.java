package com.sunbeam.dtos;

public class UserRatingDTO {
	private int userId;
	private float rating;
	public UserRatingDTO() {
	}
	public UserRatingDTO(int userId, float rating) {
		this.userId = userId;
		this.rating = rating;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	@Override
	public String toString() {
		return String.format("UserRatingDTO [userId=%s, rating=%s]", userId, rating);
	}
}
