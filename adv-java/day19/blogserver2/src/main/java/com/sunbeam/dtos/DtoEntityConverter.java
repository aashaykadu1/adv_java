package com.sunbeam.dtos;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartException;

import com.sunbeam.entities.Blog;
import com.sunbeam.entities.BlogAttachment;
import com.sunbeam.entities.BlogComment;
import com.sunbeam.entities.BlogRating;
import com.sunbeam.entities.User;

@Component
public class DtoEntityConverter {
	public UserDTO toUserDto(User entity) {
		UserDTO dto = new UserDTO();
		dto.setId(entity.getId());
		dto.setFirstName(entity.getFirstName());
		dto.setLastName(entity.getLastName());
		dto.setEmail(entity.getEmail());
		dto.setPassword(entity.getPassword());
		return dto;
	}

	public User toUserEntity(UserDTO dto) {
		User entity = new User();
		entity.setId(dto.getId());
		entity.setFirstName(dto.getFirstName());
		entity.setLastName(dto.getLastName());
		entity.setEmail(dto.getEmail());
		entity.setPassword(dto.getPassword());
		return entity;		
	}
	
	public BlogRatingDTO toRatingDTO(BlogRating entity) {
		if(entity == null)
			return null;
		BlogRatingDTO dto = new BlogRatingDTO();
		dto.setId(entity.getId());
		dto.setBlogId(entity.getBlog().getId());
		dto.setBlogId(entity.getBlog().getId());
		dto.setUserId(entity.getUser().getId());
		dto.setFirstName(entity.getUser().getFirstName());
		dto.setLastName(entity.getUser().getLastName());
		dto.setRating(entity.getRating());
		dto.setCreatedTimestamp(entity.getCreatedTimestamp());
		return dto;
	}
	
	public BlogCommentDTO toCommentDTO(BlogComment entity) {
		if(entity == null)
			return null;
		BlogCommentDTO dto = new BlogCommentDTO();
		dto.setId(entity.getId());
		dto.setBlogId(entity.getBlog().getId());
		dto.setUserId(entity.getUser().getId());
		dto.setFirstName(entity.getUser().getFirstName());
		dto.setLastName(entity.getUser().getLastName());
		dto.setComment(entity.getComment());
		dto.setCreatedTimestamp(entity.getCreatedTimestamp());
		return dto;
	}

	public BlogDTO toBlogDTO(Blog entity) {
		if(entity == null)
			return null;
		BlogDTO dto = new BlogDTO();
		dto.setId(entity.getId());
		dto.setUserId(entity.getUser().getId());
		dto.setFirstName(entity.getUser().getFirstName());
		dto.setLastName(entity.getUser().getLastName());
		dto.setTitle(entity.getTitle());
		dto.setDetails(entity.getDetails());
		dto.setTags(entity.getTags());
		dto.setCreatedTimestamp(entity.getCreatedTimestamp());
		dto.setLikeCount(entity.getLikesList().stream().mapToInt(bl -> bl.getType()).sum());
		List<BlogRatingDTO> ratings = entity.getRatingList().stream().map(br -> toRatingDTO(br)).collect(Collectors.toList());
		dto.setRatings(ratings);
		List<BlogCommentDTO> comments = entity.getCommentList().stream().map(bc -> toCommentDTO(bc)).collect(Collectors.toList());
		dto.setComments(comments);
		return dto;
	}

	public Blog toBlogEntity(BlogDTO dto) {
		Blog entity = new Blog();
		entity.setId(dto.getId());
		User user = new User();
		user.setId(dto.getUserId());
		entity.setUser(user);
		entity.setTitle(dto.getTitle());
		entity.setDetails(dto.getDetails());
		entity.setTags(dto.getTags());
		entity.setCreatedTimestamp(dto.getCreatedTimestamp());
		return entity;
	}

	public BlogComment toCommentEntity(BlogCommentDTO dto) {
		if(dto == null)
			return null;
		BlogComment entity = new BlogComment();
		entity.setId(dto.getId());
		entity.setBlog(new Blog(dto.getBlogId()));
		entity.setUser(new User(dto.getUserId()));
		entity.setComment(dto.getComment());
		entity.setCreatedTimestamp(dto.getCreatedTimestamp());
		return entity;
	}

	public BlogAttachment toAttachmentEntity(BlogAttachmentFormDTO dto) {
		if(dto == null)
			return null;
		BlogAttachment entity = new BlogAttachment();
		entity.setId(dto.getId());
		entity.setBlog(new Blog(dto.getBlogId()));
		entity.setCreatedTimestamp(dto.getCreatedTimestamp());
		try {
			entity.setData(dto.getDataFile().getBytes());
		} catch (Exception e) {
			throw new MultipartException("Can't convert MultipartFile to bytes : " + dto.getDataFile(), e);
		}
		return entity;
	}
}
