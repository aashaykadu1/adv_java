package com.sunbeam.dtos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BlogDTO {
	private int id;
	private int userId;
	private String title;
	private String details;
	private String tags;
	private Date createdTimestamp;
	private String firstName;
	private String lastName;
	private long likeCount;
	private List<BlogRatingDTO> ratings;
	private List<BlogCommentDTO> comments;
	public BlogDTO() {
		this.ratings = new ArrayList<>();
	}
	public BlogDTO(int id, int userId, String title, String details, String tags, Date createdTimestamp,
			String firstName, String lastName, long likeCount) {
		this.id = id;
		this.userId = userId;
		this.title = title;
		this.details = details;
		this.tags = tags;
		this.createdTimestamp = createdTimestamp;
		this.firstName = firstName;
		this.lastName = lastName;
		this.likeCount = likeCount;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}
	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public long getLikeCount() {
		return likeCount;
	}
	public void setLikeCount(long likeCount) {
		this.likeCount = likeCount;
	}
	public List<BlogRatingDTO> getRatings() {
		return ratings;
	}
	public void setRatings(List<BlogRatingDTO> ratings) {
		this.ratings = ratings;
	}
	public List<BlogCommentDTO> getComments() {
		return comments;
	}
	public void setComments(List<BlogCommentDTO> comments) {
		this.comments = comments;
	}
	@Override
	public String toString() {
		return String.format(
				"BlogDTO [id=%s, userId=%s, title=%s, details=%s, tags=%s, createdTimestamp=%s, firstName=%s, lastName=%s, likeCount=%s, ratings=%s]",
				id, userId, title, details, tags, createdTimestamp, firstName, lastName, likeCount, (ratings == null ? null : ratings.size()));
	}
}
