package com.sunbeam.sb.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Contact {
	@Column
	private String mobile;
	@Column
	private String address;
	@Column(unique = true)
	private String email;
	
	public Contact() {
	}
	
	public Contact(String mobile, String address, String email) {
		this.mobile = mobile;
		this.address = address;
		this.email = email;
	}


	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return String.format("Contact [mobile=%s, address=%s, email=%s]", mobile, address, email);
	}
}
