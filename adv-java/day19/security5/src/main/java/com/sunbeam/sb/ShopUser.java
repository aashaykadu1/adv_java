package com.sunbeam.sb;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.sunbeam.sb.entities.User;

public class ShopUser implements UserDetails {
	private static final long serialVersionUID = 1L;

	private String email;
	private String password;
	private boolean enabled;
	private Collection<GrantedAuthority> authorities;
	
	public ShopUser(User user) {
		this.email = user.getContact().getEmail();
		this.password = user.getEncPassword();
		this.enabled = user.getEnabled() != 0;
		this.authorities = user.getAuthorities()
				.stream()
				.map(role -> new SimpleGrantedAuthority(role.getRole()))
				.collect(Collectors.toList());
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.email;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}
}
