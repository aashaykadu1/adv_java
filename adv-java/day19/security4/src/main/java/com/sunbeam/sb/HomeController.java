package com.sunbeam.sb;

import java.security.Principal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.zaxxer.hikari.HikariDataSource;

@Controller
public class HomeController {
	@Autowired
	HikariDataSource ds;
	@GetMapping("/")
	public String home(Model model, Principal principal) {
		return "index";
	}


	@GetMapping("/user")
	public String user(Model model, Principal principal) {
		model.addAttribute("name", principal.getName());
		return "user";
	}

	@GetMapping("/admin")
	public String admin(Model model, Principal principal) {
		model.addAttribute("name", principal.getName());
		return "admin";
	}
}

