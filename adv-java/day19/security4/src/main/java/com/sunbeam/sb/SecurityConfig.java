package com.sunbeam.sb;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter 
{
	
	@Autowired
	private DataSource dataSource; // H2 datasource is auto created when on classpath
		// for others make settings in application.properties -- spring.datasource.url/username/password
//	@Value("${jdbcAuthentication.userQuery}")
	private String userQuery = "select email, password, enabled from users where email=?";
//	@Value("${jdbcAuthentication.authoritiesQuery}")
	private String authoritiesQuery = "select u.email, r.role from roles r inner join users u on u.id = r.user_id where u.email=?";
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication()
			.dataSource(dataSource)
			.usersByUsernameQuery(userQuery)
			.authoritiesByUsernameQuery(authoritiesQuery);
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/admin").hasRole("ADMIN") 	// highest restriction
			.antMatchers("/user").hasRole("USER")		
			.antMatchers("/", "/h2/**").permitAll();	// lowest restriction
		
		// do following settings for enable H2
		// reference: https://springframework.guru/using-the-h2-database-console-in-spring-boot-with-spring-security/
		http.csrf().disable();						
		http.headers().frameOptions().sameOrigin();
			
		http.formLogin();
	}
}
