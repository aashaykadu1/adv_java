package com.sunbeam;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class RestclientApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(RestclientApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		
		String url1 = "http://localhost:8080/rating/2/aggregate";
		String result1 = restTemplate.getForObject(url1, String.class);
		System.out.println("Aggregate Rating: " + result1);
		
		String url2 = "http://localhost:8080/user/signin";
		Credentials cred = new Credentials("nilesh@gmail.com", "nilesh");
		// RestTemplate
			// Send Request --> Credentials Java Object --> Jackson --> JSON
			// Get Response --> JSON --> Jackson --> UserResponseDTO
		UserResponseDTO result2 = restTemplate.postForObject(url2, cred, UserResponseDTO.class);
		System.out.println("SignedIn User: " + result2);
		
		//restTemplate.put()
		//restTemplate.delete();
		//restTemplate.headForHeaders();
		
		// low level access -- manually give req body and get resp body
		//restTemplate.exchange();
	}	
}
