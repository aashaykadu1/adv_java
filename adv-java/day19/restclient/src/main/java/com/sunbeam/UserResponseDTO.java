package com.sunbeam;

public class UserResponseDTO {
	private String status;
	private UserDTO data;
	
	public UserResponseDTO() {
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public UserDTO getData() {
		return data;
	}

	public void setData(UserDTO data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "UserResponseDTO [status=" + status + ", data=" + data + "]";
	}

}
