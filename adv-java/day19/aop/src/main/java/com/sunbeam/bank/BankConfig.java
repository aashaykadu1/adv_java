package com.sunbeam.bank;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = "com.sunbeam.bank")
@Configuration
public class BankConfig {
	@Bean
	public Account acc() {
		Account a = new AccountImpl(101, "Saving", 10000.0);
		return a;
	}
}
