package com.sunbeam.sb;

import org.springframework.beans.BeansException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import com.sunbeam.bank.Account;
import com.sunbeam.bank.BankConfig;
import com.sunbeam.bank.FileLoggerImpl;
import com.sunbeam.bank.Logger;

@Import({BankConfig.class})
@SpringBootApplication
public class Demo04Application implements CommandLineRunner, ApplicationContextAware {

	public static void main(String[] args) {
		SpringApplication.run(Demo04Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Account acc = (Account) context.getBean("acc");
		System.out.println("Account: " + acc.getClass().getName());
		
		System.out.println(acc);
		acc.deposit(2000);
		System.out.println(acc);
		acc.withdraw(5000);
		System.out.println(acc);
	}

	private ApplicationContext context;
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = applicationContext;
	}
}
