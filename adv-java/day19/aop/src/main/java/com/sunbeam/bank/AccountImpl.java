package com.sunbeam.bank;

public class AccountImpl implements Account {
	private int id;
	private String type;
	private double balance;
	private Logger logger;
	
	public AccountImpl() {
		this.logger = null;
	}
	public AccountImpl(int id, String type, double balance) {
		this.id = id;
		this.type = type;
		this.balance = balance;
		this.logger = null;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public Logger getLogger() {
		return logger;
	}
	
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	
	public void deposit(double amount) {
		if(logger != null)
			logger.log("deposit amount Rs. " + amount + " into account " + id);
		this.balance = this.balance + amount;
	}
	public void withdraw(double amount) {
		if(logger != null)
			logger.log("withdraw amount Rs. " + amount + " from account " + id);
		if(this.balance - amount < 0)
			throw new RuntimeException("Insufficient balance.");
		this.balance = this.balance - amount;
	}
	@Override
	public String toString() {
		return "AccountImpl [id=" + id + ", type=" + type + ", balance=" + balance + "]";
	}
}
