package com.sunbeam.bank;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AccountAspectImpl {
	@Before("execution (* Account.deposit(..))")
	public void beforeDeposit(JoinPoint jpt) {
		Account acc = (Account) jpt.getTarget();
		System.out.println("Before Deposit: Rs. " + jpt.getArgs()[0] + ", Balance = " + acc.getBalance());
	}
	
	@AfterReturning("execution (* Account.withdraw(..))")
	public void afterWithdrawSuccess(JoinPoint jpt) {
		System.out.println("Withdraw() operation sucessful.");
	}
	@AfterThrowing("execution (* Account.withdraw(..))")
	public void afterWithdrawFailure(JoinPoint jpt) {
		System.out.println("Withdraw() operation failed.");
	}
	@After("execution (* Account.withdraw(..))")
	public void afterWithdraw(JoinPoint jpt) {
		System.out.println("Withdraw() was called.");
	}
	
	@Around("execution (* Account.deposit(..))")
	public Object aroundDeposit(ProceedingJoinPoint jpt) throws Throwable  {
		System.out.println("deposit pre-processing!");
		// pre-processing
		long t1 = System.currentTimeMillis();
			// @Before is called
		Object result = jpt.proceed();
			// @After is called
		// post-processing
		long t2 = System.currentTimeMillis();
		long diff = t2 - t1;
		String method = jpt.getSignature().getName();
		System.out.println("Time taken by " + method + " in ms: " + diff);
		System.out.println("deposit post-processing!");
		return result;
	}
	
	@After("execution (* Account.set*(..))")
	public void afterSetter(JoinPoint jpt) {
		System.out.println("Setter is called: " + jpt.getSignature());
	}

	@Pointcut("execution (* Account.deposit(..)) || execution (* Account.withdraw(..))")
	public void accTransaction() {
	}
	
	@AfterReturning("accTransaction()")
	public void afterTransaction(JoinPoint jpt) {
		System.out.println("Sending Email on Transaction: " + jpt.getSignature());
	}
}