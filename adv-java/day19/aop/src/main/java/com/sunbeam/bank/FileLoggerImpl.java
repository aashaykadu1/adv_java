package com.sunbeam.bank;

import java.io.FileOutputStream;
import java.io.PrintStream;

import org.springframework.stereotype.Component;

@Component
public class FileLoggerImpl implements Logger {
	private String logFilePath;
	
	public FileLoggerImpl() {
		this.logFilePath = "application.log";
	}
	
	public String getLogFilePath() {
		return logFilePath;
	}

	public void setLogFilePath(String logFilePath) {
		this.logFilePath = logFilePath;
	}

	@Override
	public void log(String message) {
		// message --string--> PrintStream --bytes--> FileOutputStream --bytes--> file on disk
		try(FileOutputStream out = new FileOutputStream(logFilePath, true)) {
			try(PrintStream prn = new PrintStream(out)) {
				prn.println(message);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}






