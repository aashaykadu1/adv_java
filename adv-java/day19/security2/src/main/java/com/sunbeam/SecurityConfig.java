package com.sunbeam;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	/*
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser("user1").password("password1").roles("ADMIN")
			.and()
			.withUser("user2").password("password2").roles("USER")
			.and()
			.withUser("user3").password("password3").roles("ADMIN", "USER");
	}
	
	@SuppressWarnings("deprecation")
	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}
	*/
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser("user1").password("$2a$10$8diVXZ.WqUlZML9DSsQJ8OYqbjgfrTp/B.yuqcny6ZMvhKxr45e6q").roles("ADMIN")
			.and()
			.withUser("user2").password("$2a$10$v13YzakM.MF4fN5nq7zDjODp0gu0ASlDFUx5ADriRR9N9WIudvwaC").roles("USER")
			.and()
			.withUser("user3").password("$2a$10$l6qGCDf/qfdAJw0qnKwZC.ymawzbH/dswlXIMaXGLsOThUquLwAD6").roles("ADMIN", "USER");
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
}
