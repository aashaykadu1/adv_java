package com.sunbeam;

import java.security.Principal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.extras.springsecurity5.util.SpringSecurityContextUtils;

@Controller
public class HomeController {
	@RequestMapping("/")
	public String home(Model model, Principal principal) {
		if(principal != null) {
			model.addAttribute("name", principal.getName());
			model.addAttribute("principal", principal);
		} else {
			model.addAttribute("name", "Anonymous");
		}
		return "index";
	}
	
	@RequestMapping("/session")
	public String session(Model model, HttpSession session) {
		session.setAttribute("sunbeam", "Sep-2021");
		Enumeration<String> attrNames = session.getAttributeNames();
		HashMap<String, Object> attrs = new HashMap<String, Object>();
		while (attrNames.hasMoreElements()) {
			String attrName = (String) attrNames.nextElement();
			Object attrValue = session.getAttribute(attrName);
			//System.out.println(attrName + " - " + attrValue);
			attrs.put(attrName, attrValue);
		}
		model.addAttribute("attrs", attrs);
		return "session";
	}
	
	@RequestMapping("/authorities")
	public String authorities(Model model, Principal principal) {
		if(principal != null) {
			if(principal instanceof UsernamePasswordAuthenticationToken) {
				UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken)principal;
				List<String> authorities = token.getAuthorities().stream()
					.map(auth -> auth.getAuthority())
					.collect(Collectors.toList());
				model.addAttribute("authorities", authorities);
			}
		}
		return "authorities";
	}
}
