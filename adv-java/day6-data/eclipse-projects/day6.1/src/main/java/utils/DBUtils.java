package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {
	private static Connection cn;// null
	// add a static method to return SINGLETON (=single instance in the entire Java
	// App) to the caller

	// add a static method to open singleton connection instance
	public static void openConnection(String url, String userName, String pass) throws SQLException {
		if (cn == null)
			cn = DriverManager.getConnection(url, userName, pass);
	}

	public static Connection fetchConnection() throws SQLException {

		return cn;
	}

	// add a static method to close connection.
	public static void closeConnection() throws SQLException {
		if (cn != null)
			cn.close();
	}

}
