package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImpl;
import pojos.Candidate;
import pojos.Voter;

/**
 * Servlet implementation class CandidateLIstPage
 */
@WebServlet("/candidate_list")
public class CandidateListPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// set cont type , get pw , print mesg :" from candidate list page"
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			pw.print("<h4> from candidate list page </h4>");
			// how to retrieve validated user details from a session
			// get user details from HttpSession
			// get HS from WC
			HttpSession hs = request.getSession();

			System.out.println("New session " + hs.isNew());// false
			System.out.println("session ID " + hs.getId());// same JSESSIONID for the same clnt
			// get details
			Voter client = (Voter) hs.getAttribute("clnt_details");
			if (client != null) {

				// display validated user details :
				pw.print("<h5>Hello , Voter   " + client.getName() + "</h5>");
				// get candidate dao ref from session scope
				CandidateDaoImpl cDao = (CandidateDaoImpl) hs.getAttribute("candidate_dao");
				// invoke candidateDao's method to get list of candidates
				List<Candidate> candidates = cDao.getAllCandidates();
				pw.print("<h5> Candidate List </h5>");
				// dyn form generation
				pw.print("<form action='logout'>");
				pw.print("<h5>");
				for (Candidate c : candidates)
					pw.print("<input type=radio name='candidate_id' value=" + c.getCandidateId() + " >" + c.getName()
							+ "<br/>");
				pw.print("<input type=submit value='Vote'>");
				pw.print("</h5>");
				pw.print("</form>");

			}

			else
				pw.print("<h5> No Cookies !!!!! , Session Tracking failed...</h5>");
			// add a link for user's logout
			pw.print("<h5> <a href='logout'>Log Me Out</a></h5>");

		} catch (Exception e) {
			throw new ServletException("err in do-get of " + getClass(), e);
		}
	}

}
