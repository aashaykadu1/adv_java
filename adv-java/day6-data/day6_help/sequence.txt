Assignment status ?????


Today's topics

Complete Servlet Life cycle + ServletConfig
Executor Framework (used by WC to support concurrent handling of multiple client requests)
CGI Vs Servlets
Server pull
Scopes of attributes in web programming
ServletConfig vs ServletContext
Web app listener
Enter JSP


Revise 
HttpSession internals (draw a diagram for eVoting case study)

1. Complete Servlet Life cycle (including thread pool)

2 Is current web app DB independent or DB specific ? : 

How to make it atleast PARTIALLY DB independent ? 

Steps
1. web.xml
add 4 init params in a servlet
(db config : drvr class , db URL, user name ,password)

2. In Auth Servlet
Read init params : from ServletConfig

3. pass these params to DBUtils 
--Add new method --openConnection

Let Daos invoke fetchConnection , as before , to return fixed connection.


3. 
Replace client pull by server pull. (for AuthenticationServlet--->CatalogServlet)  : forward
Request Dispatching technique
refer : readme n diagrams
(copy day5.2 n replace client pull by server pull)






Find out from Java EE Docs
(Reading H.W)
request.getSession() vs request.getSession(boolean create)
GenericServlet's init() vs init(ServletConfig cfg)
request.getRequestDispatcher vs ServletContext.getRequestDispatcher (later!)
RequestDispatcher (forward) vs redirect