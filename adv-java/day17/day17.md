# Java EE

## Agenda
* @Transactional
* JPA Concepts
* Spring Boot JPA Integration
* Spring Data JPA Architecture
* Using Spring Data JPA in Web MVC application
* Auto-generated primary key
* Hibernate/JPA Associations
    * One to Many
    * Many to One
    * Many to Many
    * One to One
* HQL/JPQL Joins

### @Transactional
* @Transactional internally use TransactionManager to handle the transactions. It internally uses Aspect Oriented Programming using Java proxies.
    
    ```Java
    @Service
    public class UserServiceImpl {
        @Autowired
        private UserDao userDao;

        @Transactional
        public User findUserByEmail(String email) {
            User user = userDao.findByEmail(email);
            return user;
        }

        @Transactional
        public User authenticateUser(String email, String password) {
            User user = this.findUserByEmail(email);
            if(user != null && user.getPassword().equals(password))
                return user;
            return null;
        }
    }
    ```

### Using Spring Data JPA in Web MVC application

#### mvctutorials6 (Spring MVC + Spring Data)
* Copy mvctutorials5 as mvctutorials6. Rename artifactId & name in pom.xml and update the Maven project. Add the starter Spring Data JPA (in pom.xml). Remove Spring JDBC starter.
* In application.properties add database settings (if not added already).
* Create User entity class (if not present) and do ORM mapping.
* Create UserDao repository interface. Add required methods e.g. findByEmail().
* Delete UserDaoImpl & UserRowMapperImpl (Spring JDBC implementation).
* Modify UserServiceImpl to autowire and use UserDao instead of JDBC implement.
* Browser: http://localhost:8080/

### Unit testing

#### mvctutorials6 (Unit test dao & service layer)
* In User POJO add @GeneratedValue(strategy = GenerationType.IDENTITY) on id column to auto-generate id.
* Create class UserDaoTest under src/test/java for Unit Testing of UserDao. Add @Test method testFindByEmail() and testSave().
* Create class UserServiceTest under src/test/java for Unit Testing of UserService. Add @Test method testFindByEmail() and testAuthenticateUser().
* @Test method should be marked with @Transactional for advanced queries and DML operations. 
* Similar steps will be followed for TutorialDao and TopicDao testing.

### Hibernate Associations

#### mvctutorials6 (OneToMany and ManyToOne)
* Create Topic and Tutorial pojo class. Add appropriate ORM annotations.
* In Tutorial class add "topic" field with annotation @ManyToOne and @JoinColumn(name="topic_id"). Here "topic_id" is Foreign key "column" name in the "tutorials" table.
* In Topic class add "tutorialList" field with annotation @OneToMany(mappedBy=topic") and corresponding getter/setter methods. Here "topic" is field name in the Tutorial class.
* Create Spring Repository interfaces TopicDao and TutorialDao. Add required methods.
* Under src/test/java create a TutorialDaoTest class. Implement method testFindById().
	* Observe SQL queries generated. Only single JOIN query is executed. Note that default fetch type is "eager" for ManyToOne.
	* In Tutorial class change @ManyToOne annotation to add fetch=FetchType.LAZY. Run application again. Observe SQL queries generated. Observe SQL queries generated. Two queries are executed (for Tutorial and its Topic).
* Under src/test/java create a TopicDaoTest class. Implement method testFindById().
	* Observe SQL queries generated. Two queries are executed (for Topic and its Tutorials). Note that default fetch type is "lazy" for OneToMany.
	* In Topic class change @OneToMany annotation to add fetch=FetchType.EAGER. Run application again. Observe SQL queries generated. Only single JOIN query is executed.
* Remove old TopicDaoImpl, TopicRowMapperImpl, TutorialDaoImpl, and TutorialRowMapperImpl. Modify TutorialServiceImpl to use new interfaces TopicDao and TutorialDao. Finally in .jsp pages, replace ${tutorial.topic} with ${tutorial.topic.id}.

#### mvctutorials6 (Joins)
* In TutorialDao interface add custom methods for the joins.
* Write corresponding @Test methods in TutorialDaoTest.

#### mvctutorials6 (CascadeType)
* In Topic class, in @OneToMany tutorialList add cascade={ CascadeType.PERSIST, CascadeType.REMOVE}.
* Write corresponding @Test methods in TopicDaoTest.
* Note that @Test methods automatically rollback the changes. Use @Rollback(false) to keep changes in persistent.

#### CascadeType types
* PERSIST
	* When parent entity is inserted/persisted (session.persist()), then all child entities in it are also inserted/persisted (session.persist())
* REMOVE
	* When parent entity is deleted/removed (session.remove()), then all child entities in it are also deleted/removed (session.remove()). To avoid orphan rows/entities, child entities are deleted before parent entities.
* MERGE
	* When parent entity is added into the session cache (session.merge()), then all child entities in it are also added into the session cache (session.merge())
* DETACH
	* When parent entity is removed from the session cache (session.detach()), then all child entities in it are also removed from the session cache (session.detach())
* REFRESH
	* When parent entity is re-selected from database (session.refresh()), then all child entities in it are also re-selected from database (session.refresh())
* ALL
	* combination of all above.
