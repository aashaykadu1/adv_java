package com.sunbeam;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.TopicDao;
import com.sunbeam.daos.TutorialDao;
import com.sunbeam.pojos.Topic;
import com.sunbeam.pojos.Tutorial;

@SpringBootTest
class TutorialDaoTest {
	@Autowired
	private TutorialDao tutorialDao;
	
	@Transactional
	@Test
	void testFindById() {
		Optional<Tutorial> tutOpt = tutorialDao.findById(1);
		Tutorial tut = tutOpt.orElse(null);
		System.out.println(tut);
		System.out.println(tut.getTopic());
	}
	
	@Transactional
	@Test
	void testFindAllTutorialNameTopicName1() {
		List<Object[]> list = tutorialDao.findAllTutorialNameTopicName1();
		list.forEach(arr -> System.out.println(arr[0] + " - " + arr[1]));
	}
	
	@Transactional
	@Test
	void testFindAllTutorialNameTopicName2() {
		List<Object[]> list = tutorialDao.findAllTutorialNameTopicName2();
		list.forEach(arr -> System.out.println(arr[0] + " - " + arr[1]));
	}
	
	@Transactional
	@Test
	void testFindAllTutorialNameTopicName3() {
		List<Object[]> list = tutorialDao.findAllTutorialNameTopicName3();
		list.forEach(arr -> System.out.println(arr[0] + " - " + arr[1]));
	}
	
	@Transactional
	@Test
	void testFindAllTutorialName() {
		List<String> list = tutorialDao.findAllTutorialName();
		list.forEach(System.out::println);
	}
}













