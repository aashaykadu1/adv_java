package com.sunbeam;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.sunbeam.daos.UserDao;
import com.sunbeam.pojos.User;

@SpringBootTest
class UserDaoTest {
	@Autowired
	private UserDao userDao;
	
	@Test
	void testFindByEmail() {
		User user = userDao.findByEmail("rama@gmail.com");
		System.out.println("Found: " + user);
		assertNotNull(user);
	}

	@Test
	void testFindAll() {
	}

	@Test
	void testSave() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = sdf.parse("2021-11-10");
			User user = new User(0, "Nitin", "nitin@gmail.com", "nitin", 1000.0, date, "customer"); // keep id = 0 or do not assign i.e. use another param ctor without id
			User savedUser = userDao.save(user);
			System.out.println(savedUser);
			assertNotEquals(0, savedUser.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
