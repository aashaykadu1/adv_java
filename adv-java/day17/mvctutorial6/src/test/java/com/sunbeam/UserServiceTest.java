package com.sunbeam;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.sunbeam.daos.UserDao;
import com.sunbeam.pojos.User;
import com.sunbeam.services.UserServiceImpl;

@SpringBootTest
class UserServiceTest {
	@Autowired
	private UserServiceImpl userService;
	
	@Test
	void testFindByEmail() {
		User user = userService.findByEmail("rama@gmail.com");
		System.out.println("Found: " + user);
		assertNotNull(user);
	}

	@Test
	void testAuthenticateUser() {
		User user = userService.authenticateUser("rama@gmail.com", "1234");
		System.out.println("Found: " + user);
		assertNotNull(user);
	}
}
