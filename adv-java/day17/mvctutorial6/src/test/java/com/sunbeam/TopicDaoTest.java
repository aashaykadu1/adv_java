package com.sunbeam;

import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.TopicDao;
import com.sunbeam.pojos.Topic;
import com.sunbeam.pojos.Tutorial;

@SpringBootTest
class TopicDaoTest {
	@Autowired
	private TopicDao topicDao;
	
	@Transactional
	@Test
	void testFindById() {
		Optional<Topic> topicOpt = topicDao.findById(4);
		Topic topic = topicOpt.orElse(null);
		System.out.println(topic);
	//	for (Tutorial tutorial : topic.getTutorialList())
	//		System.out.println(tutorial);
	}
	
	@Rollback(value = false)
	@Transactional
	@Test
	void testSave() {
		Topic topic = new Topic(0, "Spring Data");
		Tutorial tut1 = new Tutorial(0, "Query methods", "Nilesh", new Date(), 0, "Spring Data auto generate JPQL queries as per keywords.");
		Tutorial tut2 = new Tutorial(0, "Custom queries", "Nilesh", new Date(), 0, "Use @Query to write custom JPQL or SQL queries.");
		topic.addTopic(tut1);
		topic.addTopic(tut2);
		topicDao.save(topic);
	}
	
	@Rollback(value = false)
	@Transactional
	@Test
	void testRemove() {
		Optional<Topic> topicOpt = topicDao.findById(6);
		Topic topic = topicOpt.orElse(null);
		topicDao.delete(topic);
	}	
}



























