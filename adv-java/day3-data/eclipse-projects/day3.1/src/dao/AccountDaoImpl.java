package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import static utils.DBUtils.fetchConnection;

public class AccountDaoImpl implements IAccountDao {
	// data
	private Connection cn;
	private CallableStatement cst1;

	// def ctor : init time jobs : get cn , create cst
	public AccountDaoImpl() throws ClassNotFoundException,SQLException {
		cn=fetchConnection();
		// {call <procedure-name>[(<arg1>,<arg2>, ...)]}
		String procInvocation="{call transfer_funds(?,?,?,?,?)}";
		cst1=cn.prepareCall(procInvocation);
		//register OUT param
		cst1.registerOutParameter(4, Types.DOUBLE);
		cst1.registerOutParameter(5, Types.DOUBLE);
		System.out.println("acct dao created....");
	}

	@Override
	public String transferFunds(int srcId, int destId, double amount) throws SQLException {
		// set In params
		cst1.setInt(1, srcId);
		cst1.setInt(2, destId);
		cst1.setDouble(3, amount);
		//exec the proc
		cst1.execute();
		//get results stored in OUT param
		return "Updated Src balance "+cst1.getDouble(4)+" dest balance "+cst1.getDouble(5);
	}
	public void cleanUp() throws SQLException
	{
		if(cst1 != null)
			cst1.close();
		if(cn != null)
			cn.close();
	}

}
