package tester;

import static utils.DBUtils.fetchConnection;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

public class TestPreparedStatement {

	public static void main(String[] args) {
		String sql = "select empid,name,salary,join_date from my_emp where deptid=? and join_date between ? and ?";
		try (Scanner sc = new Scanner(System.in);
				// get db connection
				Connection cn = fetchConnection();
				// create PST
				PreparedStatement pst = cn.prepareStatement(sql);

		) {
			System.out.println("Enter dept id strt date n end date(yr-mon-day)");
			// set IN params
			pst.setString(1, sc.next());// dept id
			pst.setDate(2, Date.valueOf(sc.next()));// start date
			pst.setDate(3, Date.valueOf(sc.next()));// end date
			// exec query
			try (ResultSet rst = pst.executeQuery()) {
				while (rst.next())
					System.out.printf("Emp Id %d Name %s Salary %.1f Join Date %s %n", rst.getInt(1), 
							rst.getString(2),
							rst.getDouble(3), rst.getDate(4));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
