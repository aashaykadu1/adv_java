package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import static utils.DBUtils.fetchConnection;

public class AccountDaoImpl implements IAccountDao {
	// data
	private Connection cn;
	private CallableStatement cst1;

	// def ctor : init time jobs : get cn , create cst
	public AccountDaoImpl() throws ClassNotFoundException,SQLException {
		cn=fetchConnection();
		// {call <procedure-name>[(<arg1>,<arg2>, ...)]}
		String procInvocation="{call transfer_funds(?,?,?,?,?)}";
		cst1=cn.prepareCall(procInvocation);
		System.out.println("acct dao created....");
	}

	@Override
	public String transferFunds(int srcId, int destId, double amount) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
