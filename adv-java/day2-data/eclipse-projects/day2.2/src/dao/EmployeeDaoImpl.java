package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojos.Employee;
import static utils.DBUtils.fetchConnection;

public class EmployeeDaoImpl implements IEmployeeDao {
	// state : private , non static , non transient data members
	private Connection cn;
	private PreparedStatement pst1, pst2, pst3,pst4;

	public EmployeeDaoImpl() throws ClassNotFoundException, SQLException {
		// Dao's ctor will be invoked by Tester , in a standalone app
		// get db connection
		cn = fetchConnection();
		String sql = "select empid,name,salary,join_date from my_emp where deptid=? and join_date between ? and ?";
		pst1 = cn.prepareStatement(sql);
		pst2 = cn.prepareStatement("insert into my_emp values(default,?,?,?,?,?)");
		pst3 = cn.prepareStatement("update my_emp set deptid=?,salary=salary+? where empid=?");
		pst4=cn.prepareStatement("delete from my_emp where empid=?");
		System.out.println("emp dao created...");

	}

	// add a method to close DB resources
	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		if (pst2 != null)
			pst2.close();
		if (pst3 != null)
			pst3.close();
		if (pst4 != null)
			pst4.close();
		if (cn != null)
			cn.close();
		System.out.println("emp dao cleaned up !");
	}

	@Override
	public List<Employee> getSelectedEmployees(String dept, String strtDate, String endDate) throws SQLException {
		// create empty LIst
		List<Employee> emps = new ArrayList<>();
		// set IN params
		pst1.setString(1, dept);
		pst1.setDate(2, Date.valueOf(strtDate));
		pst1.setDate(3, Date.valueOf(endDate));
		// exec query : select : execQuery
		try (ResultSet rst = pst1.executeQuery()) {
			while (rst.next())
				emps.add(new Employee(rst.getInt(1), rst.getString(2), rst.getDouble(3), rst.getDate(4)));

		} // rst.close
		return emps;// dao layer rets populated list of emps to the caller
	}

	@Override
	public String addEmpDetails(Employee newEmp) throws SQLException {
		// set IN params
		// String name, String address, double salary, String deptId, Date joinDate
		pst2.setString(1, newEmp.getName());// nm
		pst2.setString(2, newEmp.getAddress());// adr
		pst2.setDouble(3, newEmp.getSalary());// sal
		pst2.setString(4, newEmp.getDeptId());// dept
		pst2.setDate(5, newEmp.getJoinDate());// join date
		// query exec : Method of PST : public int executeUpdate() throws SQLExc
		int updateCount = pst2.executeUpdate();
		if (updateCount == 1)
			return "Emp details added....";

		return "Adding emp details failed...";
	}

	@Override
	public String updateEmpDetails(int empId, String newDept, double salIncrement) throws SQLException {
		// set IN params
		pst3.setString(1, newDept);// dept
		pst3.setDouble(2, salIncrement);// sal incr
		pst3.setInt(3, empId);
		// exec update : DML
		int updateCount = pst3.executeUpdate();
		if (updateCount == 1)
			return "Emp details updated....";
		return "Updating emp details failed...";
	}

	@Override
	public String deleteEmpDetails(int empId) throws SQLException {
		//set IN param
		pst4.setInt(1, empId);//1 => param pos 
		//DML : execute update
		int updateCount=pst4.executeUpdate();
		if (updateCount == 1)
			return "Emp details deleted....";
		return "Deleting emp details failed...";
	}
	

}
