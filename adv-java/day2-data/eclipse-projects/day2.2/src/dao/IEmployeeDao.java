package dao;

import java.sql.SQLException;
import java.util.List;

import pojos.Employee;

public interface IEmployeeDao {
//add a method declaration for getting selected emps
	List<Employee> getSelectedEmployees(String dept, String strtDate, String endDate) throws SQLException;
	//add a method declaration for inserting new emp details
	String addEmpDetails(Employee newEmp) throws SQLException;
	//add a method declaration for updating existing emp details
	String updateEmpDetails(int empId,String newDept,double salIncrement) throws SQLException;
	//add a method declaration for deleting existing emp details
	String deleteEmpDetails(int empId) throws SQLException;
}
