package tester;

import java.util.Scanner;

import dao.EmployeeDaoImpl;

public class TestLayeredApp {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			// create single dao instance : init phase of the app
			EmployeeDaoImpl dao = new EmployeeDaoImpl();
			// clnt request servicing phase
			System.out.println("Enter dept , start date n end date");
			System.out.println("Selected emps ");
			// simply call dao's method : for Data access logic
			dao.getSelectedEmployees(sc.next(), sc.next(), sc.next()).forEach(System.out::println);
			//clean up db resources
			dao.cleanUp();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
