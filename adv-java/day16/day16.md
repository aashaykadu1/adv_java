# Java EE

## Agenda
* Hibernate Session Context
* Hibernate Session Cache
* Hibernate Entity Life Cycle
* Hibernate CRUD methods
    * evict() vs detach()
    * get() vs load() vs find()
    * save() vs persist()
    * update() vs saveOrUpdate() vs merge()
    * delete() vs remove()
* Hibernate Query Language
* Spring Boot Hibernate Integration
* JPA
* Spring Data JPA
    * Architecture
    * Query Methods
    * Custom queries
* Hibernate/JPA associations
    * OneToMany
    * ManyToOne

### HTTP protocol
* Request Methods: GET vs POST
* GET
    * Browser enter any URL and Go.
    * Click Hyperlink
    * `<form method="get" ...>`
    * HTTP Redirection
* POST
    * `<form method="post" ...>`

### Spring Form Edit

#### Option 1 -- Use different urls with @RequestMapping
* /edit --> @RequestMapping("/edit") -- implement the method.
    * Create modelAttribute object and add it into model (command).
    * Display edit view (edit.jsp).
* edit.jsp
    * `<sf:form method="post" action="/update">` ...
* /update --> @RequestMapping("/update") -- implement the method.
    * Get POJO as param.
    * Update in database using service/dao layer.

#### Option 2 -- Use same url with @GetMapping and @PostMapping
* /edit --> @GetMapping("/edit") -- implement the method.
    * Create modelAttribute object and add it into model (command).
    * Display edit view (edit.jsp).
* edit.jsp
    * `<sf:form method="post" action="/edit">` ...
* /edit --> @PostMapping("/edit") -- implement the method.
    * Get POJO as param.
    * Update in database using service/dao layer.

### Session context

#### Session session = factory.openSession()
* Creates a new Hibernate session associated with a JDBC connection.
* Progammer must close the session (to avoid resource leak).
* Get SELECT access to database and for DML operations need to start a transaction.

#### Session session = factory.getCurrentSession()
* Get hibernate session associated with current context, if available.
* If no hibernate session is available in current context, create new session and attach it to the context.
* Returns the same hibernate session if called multiple times, in the same context.
* Session context
	* thread --> Hibernate session is attached to the current thread (Thread Local Storage).
	* jta --> Java Transaction API (supported by application servers like JBoss or frameworks like Open-JTA).
	* custom --> User defined (depends on implementation)
* `<property name="hibernate.current_session_context_class">thread</property>`
* When session context is closed, session is automatically closed. Progammer should not close session explicitly.
* Must start Transaction for each database operation (SELECT + DML).

#### Demo 11 (Hibernate CRUD operations)
* Copy Demo 10 as Demo 11. Rename artifactId & name in pom.xml and update the Maven project.
* Modify HbUtil class to create singleton SessionFactory (Hibernate 5 bootstrapping). 
* In hibernate.cfg.xml, set current_session_context_class=thread.
* In HbUtil class add helper methods for transaction management.
* Modify BookDaoImpl class methods findById(), save(), update(), deleteById(), findAll() to remove transaction management code.
* Modify all test code in main() to add transaction management.

### Hibernate Session Cache
* Refer slides for the concepts.

#### Demo 11 (Continued)
* In main() try to get same entity twice (with same id) in the same transaction (using Dao layer). Check if SQL query is executed twice? 

### Hibernate Entity Life Cycle
* Refer slides for the concepts.

### Hibernate CRUD methods
* get():
	* Hibernate method to find by ID (PK) -- WHERE id = ?
	* Eager loading (Immediately execute SELECT query, populate object and return it)
* find():
	* JPA compliant method
	* Same as get()
* load():
	* Hibernate method to find by ID (PK) -- WHERE id = ?
	* Lazy loading
		* Returns entity proxy (containing id)
		* When other fields are accessed, execute SELECT query and populate object.
		* If transaction is closed at that time, then throw LazyInitializationException.
		* This can be resolved by creating new session & transaction, when fields are accessed. For this set hibernate.enable_lazy_load_no_trans=true.
* save()
	* Hibernate method to insert a new row in database.
	* Auto-generate ID and execute INSERT query on database.
	* Returns Primary key of new row.
* persist()
	* JPA compliant method.
	* Execute INSERT query on database while commiting transaction.
* saveOrUpdate()
	* Hibernate method to insert or update row in database.
	* First execute SELECT query to check if row is present in database (for given id).
	* If present, execute UPDATE query; otherwise execute INSERT query()
* merge()
	* JPA compliant method
	* Similar to saveOrUpdate()
* delete()
	* Hibernate method to delete the row from the database.
* remove()
	* JPA compliant method
	* Same as delete()
* evict()
	* Hibernate method to remove the object from the session cache.
* detach()
	* JPA compliant method
	* Same as evict()
* clear()
	* Removes all objects from session cache.

#### Demo 11 (Continued)
* In BookDaoImpl, add methods to findById() -- find(), getById() -- get(), loadById() -- load().
* In main(), call these methods and print the results.
* Try to print the results outside the transaction. Note the exception.
* Configure hibernate.enable_lazy_load_no_trans = true to avoid the exception.

### Hibernate Query Language
* Hibernate (Classes/Fields) is abstraction over JDBC/RDBMS (Tables/Columns).
* Basic CRUD
	* get() --> find by id -- SELECT (single)
	* persist() --> insert new record -- INSERT (single)
	* delete() --> delete the record -- DELETE (single)
	* update() --> update the record -- UPDATE (single)
* HQL query language enable find by non-id, find multiple records with custom where clause & order by, grouping & joins, DML on multiple records.
* HQL --> Entity (class) and SQL --> Table (RDBMS).

	```Java
	String hql = "...";
	Query<EntityClass> q = session.createQuery(hql);
	// execute the query -- q.getResultList(), q.getSingleResult(), q.executeUpdate().
	```

#### HQL queries

##### SELECT operation
* default operation
* "select alias from EntityClass alias"
* "from EntityClass alias"
	* Default operation is SELECT.
	* Default fetch all columns mapped in EntityClass (@Column).
	* from EntityClass alias --> Hibernate --> SELECT col1, col2, ... FROM RdbmsTable alias.
	* "from Customer c" --> get all customers
	* "from Book b" --> get all books
* "from EntityClass alias where condition"
	* Default operation is SELECT.
	* Default fetch all columns mapped in EntityClass (@Column).
	* from EntityClass alias --> Hibernate --> SELECT col1, col2, ... FROM RdbmsTable alias WHERE condition.
	* "from Customer c where c.email = :p_email"
		* ?1 (numbered parameter) OR :p_email (named parameter)
			* q.setParameter("p_email", email);
			* q.getSingleResult()
		* JDBC: SELECT * FROM customers c WHERE c.email = ?
			* stmt.setString(1, email)
			* rs = stmt.executeQuery()
	* "from Book b where b.subject = :p_subject"
		* p_subject (named parameter)
			* q.setParameter("p_subject", subject)
			* q.getResultList()
* SELECT Examples:
	* "from Customers c order by c.address"
	* "from Book b where b.subject = :p_subject order by b.name desc"
* Advanced SELECT Examples:
	* "select new Book(id, name, price) FROM Book b"
		* SQL --> SELECT id, name, price FROM books b
		* Selected columns will be picked into Book object. Book class MUST have a constructor with three argument (id, name and price).
	* "select b.subject, SUM(b.price) FROM Book b group by b.subject"
		* SQL --> SELECT b.subject, SUM(b.price) FROM books b GROUP BY b.subject
		* `List<Object[]> list = q.getResultList();`
			* To collect the result of arbitrary queries (any columns -- not mapped in Entity) use Object[]
			* Object[] will have numbre of columns fetched in the query.
			* In this example: Object[] will be array of two elements (Object)
				* 0th element = subject
				* 1st element = sum/total price

##### UPDATE operation
* To update single record for given ID, use session.update()
* To update multiple rows use HQL.
* "update Book b set b.price = b.price + b.price * 0.05 where b.subject = :p_subject"

	```Java
	String hql = "update Book b set b.price = b.price + b.price * 0.05 where b.subject = :p_subject";
	Query q = session.createQuery(hql);
	q.setParameter("p_subject", subject);
	int count = q.executeUpdate();
	```

##### DELETE operation
* To delete single record for given ID, use session.delete() or session.remove()
* To delete multiple rows use HQL.
* "delete from Book b where b.subject = :p_subject"

	```Java
	String hql = "delete from Book b where b.subject = :p_subject";
	Query q = session.createQuery(hql);
	q.setParameter("p_subject", subject);
	int count = q.executeUpdate();
	```

##### INSERT operation
* To insert single record use session.save() or session.persist()
* HQL insert only support `INSERT INTO ... SELECT FROM ...` kind of operations.

#### Demo 12
* Copy Demo 11 as Demo 12. Rename artifactId and name in pom.xml. Update Maven project.
* In BookDao keep basic CRUD methods.
* Add HQL example methods in DAO and test them in main(). As discussed in the class.

### Spring Boot Hibernate Integration
* Refer slides for the concepts.
* spring-orm is wrapper on hibernate and provides util beans like DataSource, LocalSessionFactoryBean and HibernateTransactionManager.
	* DataSource: javax.sql.DataSource
		* Any class implemented from this interface is a valid DataSource bean e.g. DriverMangerDataSource (wrapper on JDBC), BasicDataSource (tomcat connection pool), c3p0 hibernate data source, Hikari CP, etc.
	* SessionFactory: org.hibernate.SessionFactory
		* Hibernate has internal implementation of SessionFactory (in bare Hibernate created in HbUtil).
		* Spring provides LocalSessionFactoryBean class as wrapper on SessionFactory. It is used as singleton bean to create SessionFactory.
			* hibernateProperties --> all hibernate related settings (hibernate.cfg.xml -- hibernate.xxx).
			* dataSource --> database connection
			* packagesToScan --> list of packages to be scanned for hibernate entities @Entity.
	* TrasactionManger: 
		* In hibernate Transaction is mandetory.
		* Spring provides HibernateTransactionManager that automates transation management.
			* sessionFactory --> so that TransactionManager can get the session reference internally (factory.getCurrentSession())

#### Demo 13 (Console application)
* step 1. New -- Spring Starter Project
	* Fill project details
	* Spring Starter Dpendencies
		* Spring Data JPA
		* MySQL Driver
	* Finish
* step 2. In application.properties file. Add database & hibernate properties in it (e.g. hibernate.dialect in class). You may hardcode values in HibernateConfig class (as done in class).
* step 3. Create HibernateConfig class.
	* Mark it @Configuration, @EnableTransactionMangement and @PropertySource("classpath:database.properties")
	* Add fields to get values from properties file using @Value.
	* Create beans (@Bean) dataSource, sessionFactory and transactionManager.
* step 4. Create Customer and Book pojo class. Add appropriate ORM annotations.
* step 5. Create BookDaoImpl and CustomerDaoImpl classes. Mark them as @Repository and @Transactional.
* step 6. @Autowired dao classes in main class and test the dao methods.

#### @Transactional
* Without spring
	```Java
	try {
		//beginTransaction()
		dao.method();
		//commit()
	} catch(Exception ex) {
		//rollback
	}
	```
* Spring provides @Transactional annotation. If this annotation is used on any method, spring does automate transaction management.
	* Internally it calls beginTransaction before method is called. // pre-processing
	* After the method it calls commit, if method is successful. // post-processing
	* After the method it calls rollback, if method throws any exception. // post-processing
	* Spring use AOP for implementing this.
* If @Transactional is used on a class, it applies to each method in the class.
* Refer slides.
* Internals
    * Service layer
        ```Java
        @Service
        class MyService {
            @Transactional
            public void method() {
                // ...
            }
        }
        ```
    * Main ---> Proxy (does Transaction management using TransactionManager) ---> MyService.method()
    * Proxy (Internals) ---> MyService
        ```Java
        try {
            txManager --> new transaction
            myService.method(...);
            txManager --> commit()
        } catch(Exception ex) {
            txManager --> rollback()
        }
        ```

### Spring Data JPA

#### Demo 14 (Spring Data)
* New -- Spring Starter Project
	* Fill project details
	* Spring Starter Dpendencies
		* Spring Data JPA
		* MySQL Driver
	* Finish
* In application.properties add database/hibernate properties.
* Create entity class Book (in sub-package of main class).
* Create BookDao interface (with no methods) inherited from JpaRepository.
* Implement BookServiceImpl class. Mark it as @Component and @Transactional. Autowire BookDao in it.
* In BookServiceImpl class add method findById(). Call method from main(). Observe generated SQL query.
* In BookServiceImpl class add method save(). Call method from main(). Observe generated SQL query.
* In BookDao interface add List<Book> findBySubject(String subject). (method naming convention must be strictly followed). In BookServiceImpl add findBySubject().  Call method from main(). Observe generated SQL query. 
* In BookDao interface add List<Book> findBySubjectAndAuthor(String subject, String author). (method naming convention must be strictly followed). In BookServiceImpl add findBySubjectAndAuthor(). Call method from main(). Observe generated SQL query. 
