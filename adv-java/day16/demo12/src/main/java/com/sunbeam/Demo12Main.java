package com.sunbeam;

import java.util.List;

public class Demo12Main {
	public static void main(String[] args) {
		/*
		try(BookDaoImpl dao = new BookDaoImpl()) {
			HbUtil.beginTransaction();
			List<Book> list = dao.findAll();
			list.forEach(System.out::println);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
	
		/*
		try(BookDaoImpl dao = new BookDaoImpl()) {
			HbUtil.beginTransaction();
			List<Book> list = dao.findByAuthor("KANETKAR");
			list.forEach(System.out::println);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		try(BookDaoImpl dao = new BookDaoImpl()) {
			HbUtil.beginTransaction();
			List<Book> list = dao.findBySubject("C");
			list.forEach(System.out::println);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
	
		/*
		try(BookDaoImpl dao = new BookDaoImpl()) {
			HbUtil.beginTransaction();
			List<Book> list = dao.findBySubjectAndAuthor("C", "KANETKAR");
			list.forEach(System.out::println);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		try(BookDaoImpl dao = new BookDaoImpl()) {
			HbUtil.beginTransaction();
			List<Object[]> list = dao.findAllIdTitlePrice();
			list.forEach(arr -> System.out.println(arr[0] + ", " + arr[1] + ", " + arr[2]));
			//for(Object[] arr : list)
			//	System.out.println(arr[0] + ", " + arr[1] + ", " + arr[2]);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		try(BookDaoImpl dao = new BookDaoImpl()) {
			HbUtil.beginTransaction();
			List<Book> list = dao.findAllIdTitleAuthor();
			list.forEach(System.out::println);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		try(BookDaoImpl dao = new BookDaoImpl()) {
			HbUtil.beginTransaction();
			List<Object[]> list = dao.findBookSummary();
			list.forEach(arr -> System.out.println(arr[0] + ", " + arr[1]));
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		/*
		try(BookDaoImpl dao = new BookDaoImpl()) {
			HbUtil.beginTransaction();
			int count = dao.incBookPriceOfSubject("Novell");
			System.out.println("Updated rows: " + count);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		try(BookDaoImpl dao = new BookDaoImpl()) {
			HbUtil.beginTransaction();
			int count = dao.deleteBySubject("Novell");
			System.out.println("Deleted rows: " + count);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}

		HbUtil.shutdown();
	}
}




