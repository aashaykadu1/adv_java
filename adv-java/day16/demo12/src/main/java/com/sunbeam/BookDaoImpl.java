package com.sunbeam;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

public class BookDaoImpl implements AutoCloseable {
	private Session session;
	
	public BookDaoImpl() {
		SessionFactory factory = HbUtil.getSessionFactory();
		session = factory.getCurrentSession();
	}
	
	public void close() {
	}
	
	public Book findById(int id) {
		return session.find(Book.class, id);
	}

	public List<Book> findAll() {
		//String sql = "SELECT * FROM books b";	// SQL --> Table & Column names
		//String hql = "SELECT b FROM Book b";	// HQL --> Entity & Field names
		String hql = "FROM Book b";				// HQL --> Default operation = SELECT, Default columns = all mapped columns @Column
		//String hql = "FROM com.sunbeam.Book b"; // HQL --> You may write fully qualified class name (required if multiple entity classes with same name)
		//String hql = "FROM " + Book.class.getName() + " b"; 
		Query<Book> q = session.createQuery(hql);
		return q.getResultList();
	}
	
	public List<Book> findByAuthor(String author) {
		String hql = "FROM Book b WHERE b.author = ?1";
		Query<Book> q = session.createQuery(hql);
		q.setParameter(1, author);
		return q.getResultList();
	}
	
	public List<Book> findBySubject(String subject) {
		String hql = "FROM Book b WHERE b.subject = :p_subject";
		Query<Book> q = session.createQuery(hql);
		q.setParameter("p_subject", subject);
		return q.getResultList();
	}
	
	public List<Book> findBySubjectAndAuthor(String subject, String author) {
		String hql = "FROM Book b WHERE b.subject = :p_subject AND b.author = :p_author";
		Query<Book> q = session.createQuery(hql);
		q.setParameter("p_subject", subject);
		q.setParameter("p_author", author);
		return q.getResultList();
	}
	
	// Projection in HQL
	public List<Object[]> findAllIdTitlePrice() {
		String hql = "SELECT b.bookId, b.title, b.price FROM Book b";
		Query<Object[]> q = session.createQuery(hql); // row --> { id, title, price }
		return q.getResultList();
	}
	
	public List<Book> findAllIdTitleAuthor() {
		String hql = "SELECT new Book(b.bookId, b.title, b.author) FROM Book b";
		Query<Book> q = session.createQuery(hql); // row --> new Book(id, title, author)
		return q.getResultList();
	}
	
	public List<Object[]> findBookSummary() {
		String hql = "SELECT b.subject, SUM(b.price) FROM Book b GROUP BY b.subject";
		Query<Object[]> q = session.createQuery(hql); // row --> Object[2] --> { subject, sum }
		return q.getResultList();
	}
	
	public int incBookPriceOfSubject(String subject) {
		String hql = "UPDATE Book b SET b.price = b.price + 0.05 * b.price WHERE b.subject = :p_subject";
		Query q = session.createQuery(hql);
		q.setParameter("p_subject", subject);
		return q.executeUpdate();
	}
	
	public int deleteBySubject(String subject) {
		String hql = "DELETE FROM Book b WHERE b.subject = :p_subject";
		Query q = session.createQuery(hql);
		q.setParameter("p_subject", subject);
		return q.executeUpdate();
	}
	
	public void save(Book b) {
		session.persist(b);
	}
	
	public void merge(Book b) {
		session.merge(b);
	}
	
	public void deleteById(int id) {
		Book b = session.get(Book.class, id); 
		if(b != null)
			session.delete(b);
	}	
}













