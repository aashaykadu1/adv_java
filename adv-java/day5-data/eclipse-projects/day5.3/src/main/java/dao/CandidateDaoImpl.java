package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static utils.DBUtils.fetchConnection;

import pojos.Candidate;

public class CandidateDaoImpl implements ICandidateDao {

	private Connection cn;
	private PreparedStatement pst1;

	public CandidateDaoImpl() throws ClassNotFoundException, SQLException {
		// get cn
		cn = fetchConnection();
		pst1 = cn.prepareStatement("select * from candidates");
		System.out.println("candidate dao created...");
	}

	@Override
	public List<Candidate> getAllCandidates() throws SQLException {
		List<Candidate> list = new ArrayList<>();
		// exec query
		try (ResultSet rst = pst1.executeQuery()) {
			while (rst.next())
				list.add(new Candidate(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getInt(4)));
		}
		return list;
	}

	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		System.out.println("candidate dao cleaned up...");
	}

}
