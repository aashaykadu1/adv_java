package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pojos.Voter;

/**
 * Servlet implementation class LogoutPage
 */
@WebServlet("/logout")
public class LogoutPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//set cont type
		response.setContentType("text/html");
		try(PrintWriter pw=response.getWriter())
		{
			String id=request.getParameter("candidate_id");
			System.out.println("in logout page..."+id);//debug
			//confirm if user's conversational state is retained still ?
			//1. get session
			HttpSession session=request.getSession();
			System.out.println("Session New "+session.isNew());//false iff cookies are enabled
			System.out.println("Session ID "+session.getId());//SAME session id iff cookies are enabled
			//get user details from HS
			//send "Visit Again" : login.html : in case admin had logged in
			Voter user=(Voter)session.getAttribute("clnt_details");
			if(user != null) {
				pw.print("<h5> User Details from logout page : "+user+"</h5>");
				//chk the role 
				if(user.getRole().equals("admin"))
					pw.print("<h5><a href='login.html'>Visit Again</a></h5>");
			}
			else
				pw.print("<h5> No session Tracking : no cookies!!!!!!</h5>");
			//invalidate HttpSession
			session.invalidate();
			pw.print("<h5> You have logged out successfully.....</h5>");
			
			
			
			
		}
	}

}
