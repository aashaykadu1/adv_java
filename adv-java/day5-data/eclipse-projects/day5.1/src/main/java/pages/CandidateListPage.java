package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CandidateLIstPage
 */
@WebServlet("/candidate_list")
public class CandidateListPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// set cont type , get pw , print mesg :" from candidate list page"
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			pw.print("<h4> from candidate list page </h4>");
			// how to retrieve validated user details from a cookie (req header)
			// step 3 : get all cookies , sent from the clnt
			// API of HttpServletRequest : public Cookie[] getCookies()
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
				for (Cookie c : cookies)
					if (c.getName().equals("clnt_info")) {
						// display validated user details :
						pw.print("<h5>Validated Voter Details   " + c.getValue() + "</h5>");
						break;
					}

			} else
				pw.print("<h5> No Cookies !!!!! , Session Tracking failed...</h5>");

		}
	}

}
