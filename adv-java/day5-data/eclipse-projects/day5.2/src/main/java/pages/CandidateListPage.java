package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pojos.Voter;

/**
 * Servlet implementation class CandidateLIstPage
 */
@WebServlet("/candidate_list")
public class CandidateListPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// set cont type , get pw , print mesg :" from candidate list page"
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			pw.print("<h4> from candidate list page </h4>");
			// how to retrieve validated user details from a session
			// get user details from HttpSession
			// get HS from WC
			HttpSession hs = request.getSession();
			System.out.println("from candidate list page ");
			System.out.println("New session " + hs.isNew());// false
			System.out.println("session ID " + hs.getId());// same JSESSIONID for the same clnt
//get details
			Voter client = (Voter) hs.getAttribute("clnt_details");
			if (client != null) {

				// display validated user details :
				pw.print("<h5>Validated Voter Details   " + client + "</h5>");

			}

			else
				pw.print("<h5> No Cookies !!!!! , Session Tracking failed...</h5>");
			//add a link for user's logout
			pw.print("<h5> <a href='logout'>Log Me Out</a></h5>");

		}
	}

}
